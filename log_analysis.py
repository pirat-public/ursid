"""Log analysis functions. Currently not suitable for public use this mostly works on the author's machine
and was used in order to deduce attack paths from the CERBERE experiment logs.
Logs and attack paths and better usability of these functions may be added in the future
(for instance in a publication).
"""

import glob
import re

import chouchen.graphics as graphics
import chouchen.system_graph as sg
import json

easy_website = "Exploit Public-Facing Application: Website with command injection (easy)"
medium_website = "Exploit Public-Facing Application: Website with command injection (medium)"

vulnerable_cve = "Exploitation for privilege escalation: Vulnerable sudo version (CVE-2019-14287)"
vulnerable_pkexec = "Exploitation for privilege escalation: Vulnerable pkexec process"

txt_credentials = "Unsecured Credentials: Passwords in txt file"
bash_history_credentials = "Unsecured Credentials: Passwords in .bash_history"

# It says "debien" on all of them but I'm tired of rewriting scenarios and that one doesn't have a SIGMA rule anyway.
debian_website = "Exploit Public-Facing Application: Django directory traversal rewarding ssh key (debian)"
ubuntu_website = "Exploit Public-Facing Application: Django directory traversal rewarding ssh key (ubuntu)"


def log_analysis(input_directory: str):
    """Evaluates the path of an attacker in an instance of the scenario based on SIGMA rules and logs.

    This assumes that every procedural-level scenario is written in json in a file named scenario_{i}.json, and
    every zircolite output file of the related log analysis is written in a file named log_analysis_
    {machine_name}{i}.json.
    This will output a graphical representation of each scenario attack_path_{i}.png, with the attack path of the
    attacker being shown through which nodes and transition he has taken, and timestamps on those transitions.

    This requires an outside installation of zircolite and logs to have been processed beforehand.
    A proper implementation usable by outside sources (including logs and not just something that works on my own
    machine) might be available in the future.

    Args:
        input_directory: the directory containing the info.
    """

    # Get list of scenario and log files.
    scenario_files = glob.glob(input_directory + "scenario*")
    log_files = glob.glob(input_directory + "log_analysis*")
    # Get list of all machine names and store that in a {machine_name:[list of machine paths]} dictionary
    log_files_dict = {}
    for file in log_files:
        machine_name = file.split("_")[-2]
        if machine_name not in log_files_dict:
            log_files_dict[machine_name] = [file]
        else:
            log_files_dict[machine_name].append(file)
    # sort by number
    scenario_files.sort(key=lambda f: int(re.sub('\D', '', f)))
    for machine in log_files_dict:
        log_files_dict[machine].sort(key=lambda f: int(re.sub('\D', '', f)))

    def add_no_duplicate(list_to_check: list, element: object) -> list:
        """Adds an element to a list only if it's not contained in the list already.

        Args:
            list_to_check: the list to check.
            element: the element to maybe add.

        Returns:
            The list completed.
        """
        if element in list_to_check:
            return list_to_check
        else:
            list_to_check.append(element)
            return list_to_check

    # For each scenario:
    for i in range(len(scenario_files)):
        attack_state_nodes = []
        attack_state_transitions = []
        attack_state_secrets = []
        # Load every scenario file as SystemGraph objects.
        scenario = sg.import_scenario_from_json(scenario_files[i])
        # Look for each procedure in the logs by matching the title entry
        for machine in log_files_dict:
            with open(log_files_dict[machine][i], "r") as inFile:
                log_dic = json.load(inFile)
                for detection in log_dic:
                    for transition in scenario.transitions:
                        if "title" in detection and (
                                transition.entry_node.machine == machine or transition.exit_node.machine == machine):
                            if detection["title"] == transition.procedure:
                                timestamp = detection["matches"][0]["timestamp"]
                                # Update the procedure name to add the timestamp
                                transition.procedure = transition.procedure + "<br/> Executed at: " + "[" + \
                                                       timestamp.split()[1] + "]"

                                # Handle secrets
                                attack_state_secrets += transition.rewards
                                # Update the attacker path based on this information
                                # Not using the native AttackPath function because they check entry conditions
                                # And we might not detect every transition taken by the attacke.
                                add_no_duplicate(attack_state_transitions, transition)
                                add_no_duplicate(attack_state_nodes, transition.entry_node)
                                add_no_duplicate(attack_state_nodes, transition.exit_node)
        # Output the final image.
        attack_state = sg.AttackState(attack_state_nodes, attack_state_transitions, attack_state_secrets)
        graphics.draw_graph(scenario, attack_state, False, True, input_directory + "attack_path_" + str(i),
                            hide_requires_rewards=True)


if __name__ == '__main__':
    log_analysis("/home/pibesson/zircolite/logs/analysis/")
