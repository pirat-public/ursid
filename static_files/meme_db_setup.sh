#!/bin/bash
cd /home/postgres
sudo -u postgres psql -c "create user {username} with password '{password}'"
sudo -u postgres psql -c "alter user {username} with password '{password}'"
sudo -u postgres createdb {username}
sudo -u postgres psql -f meme_DB.sql
sudo systemctl restart postgresql