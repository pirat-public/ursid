#!/bin/bash
dns_ip={{ dns_ip }}
echo -e "$(cat /etc/systemd/resolved.conf)\n DNS=$dns_ip\n10.0.2.3\n8.8.8.8\n" | sudo tee /etc/systemd/resolved.conf
sudo systemctl restart systemd-resolved.service