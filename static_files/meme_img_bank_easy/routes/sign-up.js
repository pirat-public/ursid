var express = require('express');
var router = express.Router();
const { exec } = require('child_process');


router.get('/', function(req, res, next) {
  res.render('sign-up', {});
});


router.post('/', function(req, res, next) {
    const users = req.app.get('users');
    const email = req.body.email;
    const login = req.body.login;
    const pass = req.body.password;
    const pass_conf = req.body.password_conf;
    if (pass != pass_conf) {
        res.status(400);
        res.render('sign-up', {pwd_missmatch: true});
    } else if (users.find(el => el.email == email || el.login == login)) {
        res.status(409);
        res.render('sign-up', {already_reg: true});
    } else if (!/^[a-zA-Z]*$/.test(login)) {
        res.status(400);
        res.render('sign-up', {bad_login: true});
    } else {
        users.push({login: login, email: email, password: pass, premium: false});
        exec(`mkdir public/images/${login}`);
        req.app.set('users', users);
        res.redirect('/login');
    }
});

module.exports = router;
