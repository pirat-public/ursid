const PDFDocument = require('pdfkit');
const fs = require('fs');
const path = require('path');
var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    const db = req.app.get('db');
    const images = req.app.get('images');
    let u_images = images.filter(el => el.user == req.session.user.login);
    db.all("SELECT * FROM posters", [], (err, rows) => {
        if (err) {
            console.log(err.message);
            next({status: 500, message: "Unexpected error occured."});
        } else {
            res.render('posters', {login: req.session.user.login, posters: rows, images: u_images});
        }
    });
});


router.get('/:id', function(req, res, next) {
    const db = req.app.get('db');
    db.get(`SELECT * FROM posters WHERE posters.id=${req.params.id}`, [], (err, row) => {
        if (err) {
            console.log(err.message);
            next({status: 500, message: "Unexpected error occured."});
        } else if (row == undefined) {
            next({status: 404, message: `No poster with such id: ${req.params.id}`});
        } else {
            db.all("SELECT * FROM comments, users WHERE comments.poster=? AND comments.user=users.id", [row.id], (err, rows2) => {
                if (err) {
                    console.log(err.message);
                    next({status: 500, message: "Unexpected error occured."});
                } else {
                    res.render('poster', {login: req.session.user.login, poster: row, comments: rows2, vuln: process.env.XSS_STORED});
                }
            });
        }
    });
});

router.post('/comment', function(req, res, next) {
    console.log(req.body);
    const db = req.app.get('db');
    if (req.body.comment && req.body.comment.length <= 150) {
        if (process.env.COM_DATA_RELAYING) {
            db.get("SELECT * FROM users WHERE login=?", [req.body.user], (err, row) => {
                if (err) {
                    console.log(err.message);
                    next({status: 500, message: "Unexpected error occured."});
                } else {
                    db.run("INSERT INTO comments(user,poster,message) VALUES (?,?,?);", row.id, req.body.poster, req.body.comment);
                    res.redirect(`/posters/${req.body.poster}`);
                }
            });
        } else {
            db.get("SELECT * FROM users WHERE login=?", [req.session.user.login], (err, row) => {
                if (err) {
                    console.log(err.message);
                    next({status: 500, message: "Unexpected error occured."});
                } else {
                    db.run("INSERT INTO comments(user,poster,message) VALUES (?,?,?);", row.id, req.body.poster, req.body.comment);
                    res.redirect(`/posters/${req.body.poster}`);
                }
            });
        }
    }
});

router.post('/buy', function(req, res, next) {
    console.log(req.body);
    res.render('buy', {login: req.session.user.login, poster: req.body.poster, price: req.body.price})
});

router.post('/create', function(req, res, next) {
    console.log(req.body);
    if (fs.existsSync(path.join(__dirname, '../public/images', req.session.user.login, req.body.image))) {
        const doc = new PDFDocument();
        doc.pipe(fs.createWriteStream('out.pdf'));
        doc.fontSize(18);
        doc.text("Congratulations, you successfully applied for creating a new poster !");
        doc.moveDown();
        doc.text("You can find a preview of your poster as an attachement to this PDF.");
        doc.moveDown();
        doc.text(`Poster title: ${req.body.title}`);
        doc.moveDown();
        doc.text(`Poster price: ${req.body.price}$`);
        doc.file(path.join(__dirname, '../public/images', req.session.user.login, req.body.image));
        doc.end();
        res.contentType('application/pdf');
        fs.createReadStream('out.pdf').pipe(res);
    } else {
        res.next({status: 500, message: "Unexpected error occured."})
    }
});

module.exports = router;