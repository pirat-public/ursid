# Information

Ce site a pour vocation d'être utilisé dans le cadre de l'exercice cyber nommé CERBERE. Il présente les caractéristiques suivantes:
- vulnérable à une directory traversal
- Contient une compte utilisateur et un compte administrateur
- Contient plusieurs applications dont une nommée "notes" contenant un secret à trouver sous la forme d'une clé SSH

# Installation

Ce site a été développé en utilisant Django, et peut donc être installé et déployé rapidement au moyen d'environnements virtuels python:

1. `git clone https://gitlab.inria.fr/rbrisse/cerbere.git`
2. `cd django_website`
3. `pip3 install pipenv && pipenv install`
4. `pipenv run python3 manage.py runserver`

If you consult the [returned address](http://127.0.0.1:8000/), you should get back the website's homepage.

## ADD A SUPERUSER

1. Modify the information contained in the `.env` file
2. run the command `pipenv run python3 manage.py createsuperuser --noinput`

