from django.shortcuts import render
from django.http import HttpResponse
from django.views import generic
from .models import Note
from django.contrib.auth.mixins import LoginRequiredMixin

class IndexView(LoginRequiredMixin, generic.ListView):
    template_name = 'notes/index.html'
    context_object_name = 'latest_notes_list'

    def get_queryset(self):
        """Return the last five published notes."""
        return Note.objects.order_by('-pub_date')[:20]
