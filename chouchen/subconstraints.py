"""Architectural subconstraints definitions.
This module defines:
    - OSType and OSVersion for OS constraints.
    - AccountName, AccountGroup, AccountServices, AccountPrivilege and AccountCredentials for Account constraints.
    - SoftwareType, SoftwareVersion and SoftwarePort for Software constraints.
    - FilePath, FilePermission and FileContent for File constraints.

This module also defines the SecretPrecondition class and its 2 subclasses SecretType and SecretAmount.

Subconstraints are combined together using their fuse function during the procedural refinement process, and are
converted into strings and dictionaries at the end of this process.

"""
import intervals

import chouchen.secret_generation as sec_gen


class OSType:
    """ A class to represent OS type subconstraints and whether they are compatible or not.
    When applicable we reuse names from the "name" entry of the corresponding CPE format.
    This may also be a wildcard "*".
    Attributes:
        name (str): The name of the OS.
    """
    # Represents any OS
    WILCARD_OS = "*"
    # Means that linux X debian will be debian.
    OS_IMPLICATIONS = {"linux": ["debian:debian_linux", "canonical:ubuntu_linux"], "*":
        ["debian:debian_linux", "canonical:ubuntu_linux"]}
    # Acceptable OS versions, used to initialize constraint objects.
    # Edit this if you are sure you can deploy other versions.
    OS_VERSIONS = {"debian:debian_linux": ["11.0"],
                   "canonical:ubuntu_linux": ["16.04", "20.04"]}

    # NOTE: debian 9 and old ubuntu versions removed for python conflict reasons
    # NOTE: debian 10 removed because virtualbox mounted folder bug
    # https://github.com/fujimakishouten/vagrant-boxes/issues/1

    def __init__(self, name: str):
        self.name = name

    def is_compatible(self, other: "OSType") -> bool:
        """Checks whether 2 OS types are compatible with each other.
        For now, we say that 2 OS must be the same type to be compatible.
        If one of the 2 OS is a wildcard they are compatible.

        Args:
            other (OSType): the OSType object to compare against.

        Returns:
            True if the OS types are compatible, False otherwise.

        """
        if self.name == OSType.WILCARD_OS or other.name == OSType.WILCARD_OS:
            return True
        # might be a more elegant way to do this
        if self.name in self.OS_IMPLICATIONS:
            if other.name in self.OS_IMPLICATIONS[self.name]:
                return True
        if other.name in other.OS_IMPLICATIONS:
            if self.name in other.OS_IMPLICATIONS[other.name]:
                return True
        return self.name == other.name

    def fuse(self, other: "OSType") -> "OSType":
        """Fuses OSType subconstraints together.
        This is done using the OS_IMPLICATIONS array.
        For instance:
            - fusing linux and ubuntu would return ubuntu.
            - fusing a wildcard and any specific OS will return that OS.
            - fusing two incompatible constraints raises an error.
        Args:
            other: the subconstraint to fuse with.

        Returns:
            an OSType subconstraint corresponding to the intersection of both constraints.


        """
        assert self.is_compatible(other)
        if self.name == OSType.WILCARD_OS:
            return other
        elif other.name == OSType.WILCARD_OS:
            return self
        if self.name in self.OS_IMPLICATIONS:
            if other.name in self.OS_IMPLICATIONS[self.name]:
                return other
        if other.name in other.OS_IMPLICATIONS:
            if self.name in other.OS_IMPLICATIONS[other.name]:
                return self
        return self

    def __repr__(self) -> str:
        return str(self.name)

    def to_config_format(self) -> str:
        """Converts the OSType object into a string acceptable for the post-refinement configuration file.
        For now this is identical to the __repr__ function, but may be complexified in the future.

        Returns:
            A string compatible with the configuration format.
        """
        return str(self.name)


class Version:
    """
    A class to represent Version subconstraints and whether they are compatible or not.
    Versions are internally represented as a list of number intervals, such as [5,5] or [2, 16].
    Version objects are created by inputting a string containing every version constraint, separated by spaces.
    For instance:
        - "*" corresponds to any version.
        - "<=3.5" corresponds to any version smaller or equal to 3.5
        - ">=4 <=6" corresponds to versions between 4 and 6.
        - "==7" corresponds to version 7 only.

    Attributes:
        interval_list(list[interval]): a list of interval objects.
    """

    # TODO: >= is broken on python, because of how to_dic works
    # TODO: yeah really
    # [3.5, +inf] is converted into *
    # For now I cheated it and just did not use it

    def __init__(self, version_string: str):
        self.interval_list = self.string_to_interval(version_string)

    def __repr__(self) -> str:
        return str(self.interval_list)

    # @classmethod
    # actually never used?
    # def init_from_interval(cls, interval_list: list[intervals]) -> object:
    #     """
    #
    #     Args:
    #         interval_list: The list of intervals used to initialized
    #
    #     Returns:
    #         A Version subconstraint.
    #     """
    #     result = cls("*")
    #     result.interval_list = interval_list
    #     return result

    def is_compatible(self, other: "Version") -> bool:
        """Checks whether 2 version objects are compatible, ie that there is at least one version number that satisfies
        both.

        Args:
            other (Version): the Version object to compare against.

        Returns:

        """
        return self.is_interval_list_compatible(self.interval_list + other.interval_list)

    def to_config_format(self) -> str:
        """Converts the OSType object into a string acceptable for the post-refinement configuration file.
        When given the choice between several versions, this will return the highest version possible.

        Returns:
            A string compatible with the configuration format.
        """
        interval = self.interval_list[0]
        for other in self.interval_list:
            interval = interval.intersection(other)
        # TODO: handle >= intervals
        # ATM [3.9, inf] is converted into * which is inexact but enough for our current applications
        return str(interval.upper)

    @staticmethod
    def string_version_to_float(string_version: str) -> float:
        """Converts strings to float by removing any extra "." they have.
        This is not a perfect approach (ambiguity if a software both has a 1.82 and a 1.8.2 version), but this makes it
        possible to use intervals, and hopefully no software uses such a weird method of numbering.

        Examples:
            - string_version_to_float("1.75")) returns 1.72
            - string_version_to_float("1.8.2")) returns 1.82

        Args:
            string_version: The string object to be converted

        Returns:
            A float corresponding to this string.
        """
        # Case 1: it can be naturally converted
        try:
            return float(string_version)
        except ValueError:
            # It might be something with more than one dot, such as 1.8.2
            # We remove every dot after the first one, converting 1.8.2 to 1.82
            beginning = string_version[:2]
            ending = string_version[2:].replace(".", "").replace("-", "")
            return float(beginning + ending)

    @staticmethod
    def string_to_interval(version_string: str) -> intervals:
        """Converts a value and kind input to a list of Interval object.

        Args:
            version_string: a string representing the version constraints.

        Returns:
            output_list(list[Interval]): the list of all corresponding interval objects.

        """
        kind_list = ["<=", "==", ">="]  # strictly superior/inferior should not be necessary
        output_list = []
        version_list = version_string.split()
        for version in version_list:
            if version == "*":
                output_list.append(intervals.closed(0, intervals.inf))
            elif version[:2] not in kind_list:
                raise ValueError
            value = version[2:]
            match version[:2]:
                case "<=":
                    value = Version.string_version_to_float(value)
                    output_list.append(intervals.closed(0, value))
                case "==":
                    # Special case, we read "==8,9" as "version 8 OR 9"
                    value_list = value.split(",")
                    output_singletons = intervals.empty()
                    for value_special in value_list:
                        value_special = Version.string_version_to_float(value_special)
                        output_singletons = output_singletons | intervals.closed(value_special, value_special)
                    output_list.append(output_singletons)
                case ">=":
                    value = Version.string_version_to_float(value)
                    output_list.append(intervals.closed(value, intervals.inf))
        return output_list

    @staticmethod
    def is_interval_list_compatible(interval_list: list[intervals]) -> bool:
        """ Checks whether the interval list is compatible.
        This is done by recursively calculating the intersection of all its elements and checking if it's empty or not.

        Args:
            interval_list (list[Interval]): The list of Interval objects to check.

        Returns:
            True if the list is compatible, False otherwise.
        """
        if len(interval_list) == 0:
            return True
        head = interval_list[0]
        tail = interval_list[1:]

        def rec_intersection(h: intervals, t: list[intervals]) -> intervals:
            """Recursively does the intersection of a list of intervals.

            Args:
                h: The head of the list.
                t: The tail of the list.

            Returns:
                The intersection of all the intervals in the list.
            """
            if len(t) == 0:
                return not (h.is_empty())
            else:
                return rec_intersection(h & t[0], t[1:])

        intersection = rec_intersection(head, tail)
        return intersection

    def fuse(self, other: "Version") -> "Version":
        """
        Fuses Version subconstraints together, done by using intervals.
        For instance:
            - fusing >=3.5 and == 3.5 will return 3.5.
            - fusing >= 4.0 and <= 5.0 will return [4.0, 5.0].
            - fusing >= 4.0 and <= 3.0 will raise an error.
        Args:
            other: the subconstraint to fuse with.

        Returns:
            a Version subconstraint corresponding to the intersection of both constraints.

        """
        assert self.is_compatible(other)
        self.interval_list += other.interval_list
        final_interval = self.interval_list[0]
        for i in range(1, len(self.interval_list)):
            final_interval = final_interval & self.interval_list[i]
        self.interval_list = [final_interval]
        return self


class OSVersion(Version):
    """The OSVersion subconstraints. Reuses the Version class.
    
    """
    pass


class AccountName:
    """A class used to represent account name constraints and their compatibilities.
    For now we only check whether 2 names are equal or not, making this class fairly simple.

    Attributes:
        name (str): the name of the account.
    """

    Wildcard = "*"
    EntryUser = "ENTRY_USER"  # used in procedures to indacte values that need to be dynamically updated
    ExitUser = "EXIT_USER"  # same
    Take_Value_From_Transition_Requiring_Same_Secret = "TAKE_VALUE_FROM_TRANSITION_REQUIRING_SAME_SECRET"

    def __init__(self, name: str) -> None:
        self.name = name

    def is_compatible(self, other: "AccountName") -> bool:
        """Checks whether two AccountName constraints are compatible.
        This is done by checking that either one is a wildcard, or that they are identitcal.

        Args:
            other: The AccountName constraint to compare against.

        Returns:
            True if the constraints are compatible, False otherwise.
        """
        return self.name != other.name or self.is_wildcard() or other.is_wildcard()

    def __repr__(self) -> str:
        return str(self.name)

    def __eq__(self, other: "AccountName") -> bool:
        return self.name == other.name

    def is_wildcard(self) -> bool:
        """Checks whether this AccountName constraint corresponds to a wildcard.
        
        Returns:
            True if it's equal to a wildcard, False otherwise.

        """
        return self.name == AccountName.Wildcard


class AccountGroup:
    """A class used to represent account group constraints and their compatibilities.
    For now we only check whether 2 account group names are equal or not, making this class fairly simple.

    Attributes:
        name (str): the name of the group.
    """

    Wildcard = "*"
    EntryUser = "ENTRY_USER"
    ExitUser = "EXIT_USER"

    def __init__(self, name: str) -> None:
        self.name = name

    def __repr__(self) -> str:
        return str(self.name)

    def is_wildcard(self) -> bool:
        """Checks whether this AccountGroup constraint corresponds to a wildcard.
        
        Returns:
            True if the constraint is equal to a wildcard, False otherwise.
        """
        return self.name == AccountGroup.Wildcard

    def is_compatible(self, other: "AccountGroup") -> bool:
        """Checks whether two AccountGroup constraints are compatible.
        This is done by checking that either one is a wildcard or that they have the exact same value.
        
        Args:
            other: The constraint to check against.

        Returns:
            True if the constraints are compatible, False otherwise.
        """
        return self.name == other.name or self.is_wildcard() or other.is_wildcard()

    def fuse(self, other: "AccountGroup") -> "AccountGroup":
        """Fuses two AccountGroup constraints.
        Examples:
            - "*" and "Alice" returns Alice.
            - "*" and "*" returns "*".
            - "Bob" and "Bob" returns "Bob".
            - "Alice" and "Bob" raises an error.
        
        Args:
            other: The constraint to fuse with.

        Returns:
            The fused constraint.
        """
        if self.name == AccountGroup.Wildcard:
            return other
        elif other.name == AccountGroup.Wildcard:
            return self
        elif self.name == other.name:
            return self
        else:
            raise ValueError


class AccountPrivilege:
    """A class used to represent account privilege constraints and their compatibilities.
    For now this is represented by a single string, and can have 2 values: User or SuperUser.
    SuperUsers will get root (or equivalent) privileges on the resulting architecture.

    Attributes:
        privilege (str): the level of privilege required.
    """

    User = "USER"
    SuperUser = "SUPERUSER"
    Wildcard = "*"

    def __init__(self, privilege: str) -> None:
        assert isinstance(privilege, str)
        self.privilege = privilege

    def __repr__(self) -> str:
        return str(self.privilege)

    def is_wildcard(self) -> bool:
        """
        
        Returns:
            True if this constraint is a wildcard, False otherwise.
        """
        return self.privilege == AccountPrivilege.Wildcard

    def is_compatible(self, other: "AccountPrivilege") -> bool:
        """Checks compatibility between 2 account permissions subconstraints.
        For now this only checks that they are equal.

        Args:
            other (AccountPermissions): the permission subconstraint to check against.

        Returns:
            True if the 2 constraints are compatible, False otherwise.
        """
        return self.privilege == other.privilege or self.is_wildcard() or other.is_wildcard()

    def fuse(self, other: "AccountPrivilege") -> "AccountPrivilege":
        """Fuses two AccountPrivilege constraints.
        Examples:
            - "*" and "*" returns "*".
            - "*" and "SUPERUSER" returns "SUPERUSER".
            - "USER" and "USER" returns "USER".
            - "USER" and "SUPERUSER" raises an error.

        Args:
            other: The constraint to fuse with.

        Returns:
            The constraint resulting from the fusion.

        """
        if self.privilege == AccountPrivilege.Wildcard:
            return other
        elif other.privilege == AccountPrivilege.Wildcard:
            return self
        elif self.privilege == other.privilege:
            return self
        else:
            raise ValueError


class AccountServices:
    """A class used to represent account service constraints and their compatibilities
    For now such constraints are always compatible, this is mostly used to express that a user has to launch a specific
    service.
    Attributes:
        service_list (list[str]): The list of services
    """

    def __init__(self, service_list: list[str]) -> None:
        assert isinstance(service_list, list)
        self.service_list = service_list

    def is_compatible(self, other: "AccountServices") -> bool:
        """
        Checks whether two services constraints are compatible.
        For now this is always the case, but might be updated in the future.
        Args:
            other: The constraint to check against.

        Returns:
            True if the constraints are compatible, False otherwise.
        """
        return True  # Might be updated one day

    def __repr__(self) -> str:
        return str(self.service_list)

    def fuse(self, other: "AccountServices") -> "AccountServices":
        """Fuses two service constraints.
        This is done by just adding their list of services together.

        Args:
            other: The constraint to fuse with.

        Returns:
            The fused constraint.
        """
        self.service_list += other.service_list
        return self

    @staticmethod
    def empty_service_list() -> list:
        """Creates a list representing a default service list. For now this is just an empty list.

        Returns: An account credential object that will be compatible with any other.

        """
        return []


class AccountCredentials:
    """A class used to represent account credentials constraints and their compatibilities.
    Attributes:
        credentials (str): The account credential subconstraint
    """
    StrongRandomPassword = "STRONG_RANDOM_PASSWORD"
    WeakRandomPassword = "WEAK_RANDOM_PASSWORD"
    UnixUserAccount = "UNIX_USER_ACCOUNT"
    Wildcard = "*"

    def __init__(self, credentials: str, requires_secret: str = False, secret_type: str = None,
                 secret_amount: str = None) -> None:
        self.credentials = credentials
        self.requires_secret = requires_secret
        self.secret_type = secret_type
        self.secret_amount = secret_amount

    def __repr__(self) -> str:
        return str(self.credentials)

    def is_wildcard(self) -> bool:
        """Checks whether the constraint is a wildcard.

        Returns:
            True if the constraint is a wildcard, False otherwise.

        """
        return self.credentials == AccountCredentials.Wildcard

    @staticmethod
    def wildcard_dic() -> dict:
        """Creates a dictionary representing a wildcard account credential constraint

        Returns: A dictionary representing an account credential object that will be compatible with any other.

        """
        return {"Credential Type": AccountCredentials.Wildcard, "Requires Secret": False}

    @staticmethod
    def strong_random_password() -> dict:
        """Creates a dictionary corresponding to a StrongRandomPassword credential type. Used for testing mostly.

        Returns:
            A corresponding dictionary.
        """
        return {"Credential Type": AccountCredentials.StrongRandomPassword, "Requires Secret": False}

    @staticmethod
    def weak_random_password() -> dict:
        """Creates a dictionary corresponding to a WeakRandomPassword credential type. Used for testing mostly.

        Returns:
            A corresponding dictionary.
        """
        return {"Credential Type": AccountCredentials.WeakRandomPassword, "Requires Secret": False}

    def is_compatible(self, other: "AccountCredentials") -> bool:
        """Checks compatibility between 2 account credential subconstraints.
        For now this only checks that they are equal.

        Args:
            other (AccountCredentials): the credential subconstraint to check against.

        Returns:
            True if the 2 constraints are compatible, False otherwise.
        """
        return self.credentials == other.credentials or self.is_wildcard() or other.is_wildcard()

    def fuse(self, other: "AccountCredentials") -> "AccountCredentials":
        """ Fuses two AccountCredential constraints together.
        Examples:
            - "*" and "*" returns "*".
            - "*" and "STRONG_RANDOM_PASSWORD" returns "STRONG_RANDOM_PASSWORD".
            - "WEAK_RANDOM_PASSWORD" and "STRONG_RANDOM_PASSWORD" raises an error.

        Args:
            other: The constraint to fuse against.

        Returns:
            The fused constraint.

        """
        if self.credentials == AccountCredentials.Wildcard:
            return other
        elif other.credentials == AccountCredentials.Wildcard:
            return self
        elif self.credentials == other.credentials:
            return self
        else:
            raise ValueError

    def to_config_format(self, assignated_secrets: list[str], secret_dictionary: dict) -> dict:
        """Converts the AccountCredentials object into a string acceptable for the post-refinement configuration file.
        If the procedure requires secrets, it will go through the assignated_secrets list and create one of the
        appropriate type for each.
        This will also update the given secret_dictionary file.
        Args:
            assignated_secrets(list[string]): a list of name of assignated secrets.
            secret_dictionary(dict[sec_gen.Secret]): a dictionary of secret objects.
        Returns:
            A string compatible with the configuration format.
        """

        def is_string_and_number_compatible(string: str, number: int) -> bool:
            """Checks whether a string can be converted to either a number or is a "*" wildcard.

            Args:
                string: The string to compare with.
                number: The number to compare with.

            Returns:
                True if they are compatible, False otherwise.

            """
            if string == "*":
                return True
            if int(string) == number:
                return True
            return False

        if self.requires_secret:
            if not is_string_and_number_compatible(self.secret_amount, len(assignated_secrets)):
                raise ValueError
            final_values = []
            for secret_name in assignated_secrets:
                final_values.append(sec_gen.generate_secret(secret_name, secret_dictionary))
        else:
            final_values = [sec_gen.generate_credential(self.credentials)]
        # Note: for now we only support UnixUserAccount as a type of account credential
        # And also only 1 secret per credential constraint
        # TODO: expand on this if we find procedures that may require more
        return {"Type": "UnixUserAccount", "Value": final_values[0]}


class SoftwareType:
    """ A class used to represent software type constraints and their compatibilities.
        For now we only check whether 2 software types are equal or not, making this class fairly simple.
        Software type names are taken from the CPE format when available.

        Attributes:
            software_type (str): the type of software.
        """

    def __init__(self, software_type: str) -> None:
        self.software_type = software_type

    def __eq__(self, other: "SoftwareType"):
        return self.software_type == other.software_type

    def __repr__(self) -> str:
        return str(self.software_type)

    def to_config_format(self) -> str:
        """Converts the SoftwareType object into a string acceptable for the post-refinement configuration file.
        For now this is identical to the __repr__ function, but may be complexified in the future.

        Returns:
            A string compatible with the configuration format.
        """
        return str(self.software_type)


class SoftwareVersion(Version):
    """The subconstraint for software versions.
    Identical to the version class.

    """
    pass


class SoftwarePort:
    """ A class used to represent software port constraints and their compatibilities.
        For now we only check whether 2 software ports are equal or not, making this class fairly simple.
        A software using no port will be set at -1.
        Note: this class has both a compatibility and an equality function due to how software with no ports are
        handled. Indeed, if 2 software are the same they need to share the same ports (we use eq), but if they
        are different they need to use different ports except in the case where they both don't use any (use
        is_compatible)

        Attributes:
            port (int): the port used by the software (-1 if not applicable).
            """

    def __init__(self, port: int = -1) -> None:
        self.port = port

    def __eq__(self, other: "SoftwarePort"):
        return self.port == other.port

    def is_compatible(self, other: "SoftwarePort") -> bool:
        """Checks compatibility between two software port constraints.
        For now two software port constraints are compatible if they're equal but aren't both -1.
        This makes sense when calculating the software constraints.
        TODO: find a better name for this function (it's weird to say 2 software using no ports aren't compatible).

        Args:
            other: The file constraint to check against.

        Returns:
            True if the constraints are compatible, False otherwise.
        """
        return self.port == other.port and not (self.port == -1 and other.port == -1)

    def __repr__(self) -> str:
        return str(self.port)

    def to_config_format(self) -> str:
        """Converts the SoftwareType object into a string acceptable for the post-refinement configuration file.
        For now this is identical to the __repr__ function, but may be complexified in the future.

        Returns:
            A string compatible with the configuration format.
        """
        return str(self.port)


class FilePath:
    """A class used to represent software path constraints and their compatibilities.
        For now this only checks whether 2 paths are identical or not.

    Attributes:
        path (str): the path of the file on the machine.
    """

    def __init__(self, path: str) -> None:
        self.path = path

    def __eq__(self, other: "FilePath") -> bool:
        return self.path == other.path

    def __repr__(self) -> str:
        return str(self.path)


class FilePermission:
    """A class used to check on file permissions constraints and their compatibilities.
    For now this only checks whether 2 permissions are strictly identical or not.
    Permissions are represented using the Unix numbered format. For Windows OS, these will be
    translated into access control lists for the currently logged in user, a group containing the user, and everyone.

    Attributes:
        perm (str): the required permission for the file.
        user (str): the user that owns the file.
        group (str): the group that owns the file.
    """

    Wildcard = "*"
    EntryUser = "ENTRY_USER"
    ExitUser = "EXIT_USER"

    def __init__(self, perm: str, user: str, group: str) -> None:
        self.perm = perm
        self.user = user
        self.group = group

    def is_compatible(self, other: "FilePermission") -> bool:
        """Checks compatibility between two file permission constraints.
              For now two constraints are only compatible if they are equal or one of them is a wildcard.

              Args:
                  other: The file constraint to check against.

              Returns:
                  True if the constraints are compatible, False otherwise.
              """
        is_perm_compatible = (self.perm == FilePermission.Wildcard or
                              other.perm == FilePermission.Wildcard) or self.perm == other.perm

        is_user_compatible = (self.user == other.user)
        is_group_compatible = (self.user == other.user)
        return is_perm_compatible and is_user_compatible and is_group_compatible

    def __repr__(self) -> str:
        return str(self.perm) + ", " + str(self.user) + ", " + str(self.group)


class FileContent:
    """A class used to check on file content constraints and their compatibilities.
    Note: this class doesn't need a to_config_file method, as this is taken care of by the file_generation module.
    Attributes:
        content (str): the type of file that needs to be generated.
    """

    ADD_SSH_PUBLIC_KEY = "ADD_SSH_PUBLIC_KEY"
    SSH_ENABLE_PASSWORD = "SSH_ENABLE_PASSWORD"
    Wildcard = "*"

    def __init__(self, content: str, requires_secret: bool, secret_type: str = None, secret_amount: str = None) -> None:
        self.content = content
        self.requires_secret = requires_secret
        self.secret_type = secret_type
        self.secret_amount = secret_amount

    def is_compatible(self, other: "FileContent") -> bool:
        """Checks compatibility between two file content constraints.
        For now two constraints are only compatible if they are equal.

        Args:
            other: The file constraint to check against.

        Returns:
            True if the constraints are compatible, False otherwise.
        """
        return self.content == other.content

    def __repr__(self) -> str:
        return str(self.content)


class SecretType:
    """A class representing which kind of secret a transition may give

    Attributes:
        secret_type (str): the type of secret
        secret_subtype(str): handles special cases when we want to generate specific values while keeping compatibility
        """

    PLAINTEXT_PASSWORD = "PLAINTEXT_PASSWORD"
    SSH_KEY_PAIR = "SSH_KEY_PAIR"
    FIXED_CAMERA_PASSWORD = "FIXED_CAMERA_PASSWORD"

    def __init__(self, secret_type: str) -> None:
        self.secret_type = secret_type
        self.secret_subtype = None
        if secret_type == "FIXED_CAMERA_PASSWORD":
            self.secret_type = "PLAINTEXT_PASSWORD"
            self.secret_subtype = "FIXED_CAMERA_PASSWORD"

    def __repr__(self) -> str:
        return str(self.secret_type)

    def __eq__(self, other: "SecretType") -> bool:
        return self.secret_type == other.secret_type

    def __hash__(self) -> int:
        return hash(str(self))

    def is_compatible(self, other: "SecretType") -> bool:
        """Checks whether two SecretType objects are compatible.
       This is done by checking that they are equal. This may be complexified in the future to allow for more refined
       secret types.

       Args:
           other (SecretType): the SecretType to check against.

       Returns:
           True if they are compatible, False otherwise.
       """

        return self.secret_type == other.secret_type


class SecretAmount:
    """A class that represents how many of a single secret may be given or required by a transition.
    For now this is just a string containing either an integer or the wildcard character *.

    Attributes:
        secret_amount (str): the number of secrets that may be given or required by a transition.

    """

    WILDCARD = "*"

    def __init__(self, secret_amount: str) -> None:
        self.secret_amount = secret_amount

    def __repr__(self) -> str:
        return str(self.secret_amount)

    def is_wildcard(self) -> bool:
        """Checks whether this constraint is equal to a wilcard or not.

        Returns:
            True if the constraint is a wildcard, False otherwise.
        """
        return self.secret_amount == self.WILDCARD

    def is_compatible(self, other: "SecretAmount") -> bool:
        """Checks whether two SecretNumber objects are compatible.
        This is done by either checking that they are equal, or that one is a wildcard.

        Args:
            other (SecretNumber): the SecretNumber to check against.

        Returns:
            True if they are compatible, False otherwise.
        """
        return self.is_wildcard() or other.is_wildcard() or (self.secret_amount == other.secret_amount)


class SecretPrecondition:
    """A class for handling a single secret precondition (lists of them are handled in SecretPreconditions in
    constraints.py.
    For now this is just a secret type and a secret amount.

    Attributes:
        secret_type(SecretType): the type of secret.
        amount(SecretNumber): the amount of secrets.

    """

    def __init__(self, secret_type: str, amount: str) -> None:
        self.secret_type = SecretType(secret_type)
        self.amount = SecretAmount(amount)
