"""Graphics utility module.

This is used to convert a scenario described using the SystemGraph class and model into a proper graph representation.

Options include:
    - Showing technique names alongside their number on the transitions.
    - Show attacker state.
    - Show procedure names instead of technique names.
    - Save the graph at a given location.

In particular, refining a scenario will call this module in order to provide a graphical aid on which procedures
were taken.

"""

import graphviz

import chouchen.json_operations as json_operations
import chouchen.system_graph as system_graph

STARTING_NODE_COLOR = "darkseagreen"
WINNING_NODE_COLOR = "lightcoral"
DEFAULT_NODE_COLOR = "powderblue"
GREEN_TEXT_COLOR = "forestgreen"
BLUE_TEXT_COLOR = "deepskyblue3"
RED_TEXT_COLOR = "firebrick"
TRANSITION_TAKEN_COLOR = "#FF0082"
ATTACKER_KNOWLEDGE_COLOR = "#FF9DCF"
CONTROLLED_NODE_IMAGE_PATH = "/home/pibesson/ursid-final/imgs/hacker.png"
SAVE_PATH = "./imgs/scenario"


def draw_graph(sys_graph: system_graph, attack_state: system_graph.AttackState = system_graph.AttackState([], [], []),
               technique_names: bool = False,
               proc_mode: bool = False,
               save_mode: str = None,
               hide_requires_rewards: bool = False) -> None:
    """Provides a graphical representation of the scenario, showing off all attack positions and transitions alongside
    their attack techniques. Starting and winning nodes are also indicated.

    If an attack state is provided, nodes controlled and transitions taken by the attacker will be indicated,
    alongside the list of secrets acquired by the attacker.

    Args:
        sys_graph: The scenario to graphically represent.
        attack_state: An optional attack state.
        technique_names: Show full technique names if available (default: False)
        proc_mode: Show procedure names instead of technique names (default: False)
        save_mode (str): Saves the graph at this location instead of showing if this parameter exists (default: None)
        hide_requires_rewards: Does not show secret information on the graph (requires, rewards, attacker information)
    Returns:

    """
    graph = graphviz.Digraph('unix', format='png', filename=SAVE_PATH,
                             node_attr={'shape': 'box', "style": "filled, rounded", "fontname": "lato",
                                        "margin": "0.25", 'color': 'black'})
    for node in sys_graph.nodes:  # Draw all nodes
        if node in sys_graph.starting_nodes:
            node_color = STARTING_NODE_COLOR
        elif node in sys_graph.winning_nodes:
            node_color = WINNING_NODE_COLOR
        else:
            node_color = DEFAULT_NODE_COLOR
        if node in attack_state.nodes:
            graph.node(str(node), label='<p' + str(node.node_id) + ": " + str(node.machine) + ", " + '<I>'
                                        + str(node.user) + '</I>' + '<br/>>',
                       image=CONTROLLED_NODE_IMAGE_PATH, imagepos='br', imagescale="0.25", fillcolor=node_color,
                       penwidth="3", color=TRANSITION_TAKEN_COLOR)

        else:

            graph.node(str(node), label='<p' + str(node.node_id) + ": " + str(node.machine) + ", " + '<I>'
                                        + str(node.user) + '</I>' + '<br/>>',
                       fillcolor=node_color, penwidth="0")
    transition_index = 0
    for transition in sys_graph.transitions:  # Draw all transitions
        FONT_COLOR = GREEN_TEXT_COLOR
        if transition in attack_state.transitions:
            FONT_COLOR = TRANSITION_TAKEN_COLOR
        trans_label = '<<FONT COLOR="' + FONT_COLOR + '">' + "&#964;" + "<sub>" + str(
            transition.transition_id) + "</sub>" + ": "
        if not proc_mode:
            if technique_names:
                try:
                    trans_label += str(transition.technique) + ": " \
                                   + str(json_operations.get_technique_name(transition.technique)) + '</FONT''> <br/> '
                except KeyError:
                    trans_label += str(transition.technique) + '</FONT> <br/>'
            else:
                trans_label += str(transition.technique) + '</FONT> <br/>'
        else:
            procedure_text = str(transition.procedure)
            procedure_text = procedure_text.split(":", 1)[0] + "<br/>" + procedure_text.split(":", 1)[1]
            trans_label += procedure_text + '</FONT> <br/>'
        if transition.requires and not hide_requires_rewards and transition in attack_state.transitions:
            trans_label += '<FONT COLOR="' + TRANSITION_TAKEN_COLOR + '">' + "Requires:" + str(
                transition.requires) + '</FONT>'
        elif transition.requires and not hide_requires_rewards:
            trans_label += '<FONT COLOR="' + RED_TEXT_COLOR + '">' + "Requires:" + str(transition.requires) + '</FONT>'
        if transition.rewards and not hide_requires_rewards and transition in attack_state.transitions:
            trans_label += '<FONT COLOR="' + TRANSITION_TAKEN_COLOR + '">' + "Rewards:" + str(
                transition.rewards) + '</FONT>'
        elif transition.rewards and not hide_requires_rewards:
            trans_label += '<FONT COLOR="' + BLUE_TEXT_COLOR + '">' + "Rewards:" + str(transition.rewards) + '</FONT>'
        trans_label += ">"
        if transition in attack_state.transitions:
            graph.edge(str(transition.entry_node), str(transition.exit_node), label=trans_label,
                       color=TRANSITION_TAKEN_COLOR,
                       arrowsize="1", penwidth="2")
        else:
            graph.edge(str(transition.entry_node), str(transition.exit_node), label=trans_label)
        transition_index += 1
    # draw secrets
    secret_label = attack_state.secrets
    if secret_label and not hide_requires_rewards:
        new_secret_label = "ATTACKER KNOWLEDGE: \n" + str(secret_label)
        new_secret_label = new_secret_label
        graph.node(new_secret_label, label=new_secret_label, style="filled", color=ATTACKER_KNOWLEDGE_COLOR)
    if not save_mode:
        graph.view()
    else:
        graph.save()
        graph.render(filename=save_mode, view=False, cleanup=True)
