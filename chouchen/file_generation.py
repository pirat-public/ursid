# -*- coding: utf-8 -*-
"""File generation module.

This module is responsible for generating files needed for the scenario, such as password leaks,
copying entire directories, or writing flawed configuration files.
Note that generating files may imply generating secrets that go along with them (for instance files containing
credentials).

Examples of file generation:
    - Files are copied directly from the static_files directory.
    - Files are entirely written here.
    - Files are naturally present on the machine, and only the line that needs to be changed will be generated here.


The possible type of file edits are as follows:
    - WRITE: the file will be entirely copied onto the machine.
    - APPEND: the contents of the file will be added at the end of a file with the same path on the machine.
    - EDIT "text": the contents of this file will replace the line containing "text" of a file with the same path.

Files are generated in the GENERATED_FILE_PATH folder, which by default is ./output/scenario_0/generated_files.
If more than one scenario are generated (ie if that folder already exists), new files will be generated in
./output/scenario_i/generated_files, with scenario_i being the first available directory number.

"""

import fileinput
import os
import random as rd
import shutil

import art
from git import Repo

import chouchen.constraints
import chouchen.secret_generation as sec_gen

# GENERATED_FILE_PATH = os.path.join(ROOT_DIR, "output", "scenario", "generated_files")
GENERATED_FILE_PATH = "./output/scenario/generated_files"
GENERATED_FILE_PATH_FROM_FILE = "./generated_files"


def generate_file(file_constraint: "chouchen.constraints.FileConstraints", secret_dictionary: dict,
                  machine_name: str, user_name: str, already_generated: bool = False):
    """ Generates a new file according to file_type and returns its local path and the type of file edit it will be.
    These have to be written manually for now.
    This may also reuse values within or update the secret dictionary - for instance if a file has to leak a password.
    If already_generated is True, the file will not be regenerated and only the location + edit values will be output.

    Args:
        machine_name: The name of the related machine (useful to create directory)
        user_name: The name of the related user (useful for some files)
        secret_dictionary: the dictionary of all secrets.
        file_constraint: The type of file that needs to be generated.
        already_generated: Indicates whether the file was generated already or not.

    Returns:
        The location of the generated file and the edit to make to that file.
    """

    # Get all the secret values to put
    secret_list = []

    if not os.path.isdir(GENERATED_FILE_PATH + "/" + machine_name):
        os.mkdir(GENERATED_FILE_PATH + "/" + machine_name)
    output_directory = GENERATED_FILE_PATH + "/" + machine_name
    output_directory_from_file = GENERATED_FILE_PATH_FROM_FILE + "/" + machine_name
    for secret_name in file_constraint.assigned_secrets:
        if secret_name in secret_dictionary:
            secret_list.append(secret_dictionary[secret_name])
    # then match file type with what file to generate and where to generate it
    file_type = file_constraint.content.content
    if len(file_type.split()) > 1:  # useful to pass arguments for file construction
        file_arguments = file_type.split()[1:]
        file_type = file_type.split()[0]

    match file_type.lower():  # Useful if you wanna change the directory name from the constraint
        case "unix_pictures":
            out_path = "/Pictures"
        case "unix_documents":
            out_path = "/Documents"
        case "unix_music":
            out_path = "/Music"
        case "unix_important":
            out_path = "/IMPORTANT"
        case _:
            out_path = "/" + file_type.lower()

    out_path = output_directory + out_path
    # generate_file is called twice for every file: once with already_generated = False and once with True
    # We need to give the file a new path if a) we're not in the second call and b) the file path already exists
    already_exists = os.path.exists(out_path) and not already_generated
    i = 0
    new_out_path = ""
    if already_exists:
        while already_exists:
            new_out_path = out_path + str(i)
            already_exists = os.path.exists(new_out_path)
            i += 1
        out_path = new_out_path
    out_path_from_file = "/" + os.path.basename(out_path)
    out_path_from_file = output_directory_from_file + out_path_from_file
    already_exists = os.path.exists(out_path)
    match file_type:
        case "BASH_HISTORY_PASSWORD_LEAK":
            # create the file, adding every password from secret_name
            if not already_exists:
                with open(out_path, "w+") as outFile:
                    for secret in secret_list:
                        # add the user names as a clue
                        for node in secret.related_nodes:
                            outFile.write(
                                "#TODO store credentials securely\n " + "machine: " + node.machine + "\n user: "
                                + node.user + "\n Password:")
                        outFile.write(sec_gen.generate_secret(secret.name, secret_dictionary))
            # return the good stuff
            return out_path_from_file, "WRITE"

        case "TXT_FILE_PASSWORD_LEAK":
            # create the file, adding every password from secret_name
            if not already_exists:
                with open(out_path, "w+") as outFile:
                    for secret in secret_list:
                        # add the user names as a clue
                        for node in secret.related_nodes:
                            outFile.write(
                                "#TODO store credentials securely\n " + "machine: " + node.machine + "\n user: "
                                + node.user + "\n")
                        outFile.write(sec_gen.generate_secret(secret.name, secret_dictionary))
            # return the good stuff
            return out_path_from_file, "WRITE"

        case "ADD_SSH_PRIVATE_KEY":
            if not already_exists:
                with open(out_path, "w+") as outFile:
                    for secret in secret_list:
                        key_pair = sec_gen.generate_secret(secret.name, secret_dictionary)
                        private_key = key_pair.split("|")[0]
                        outFile.write(private_key)
            return out_path_from_file, "WRITE"

        case "ADD_SSH_PUBLIC_KEY":
            if not already_exists:
                with open(out_path, "w+") as outFile:
                    for secret in secret_list:
                        key_pair = sec_gen.generate_secret(secret.name, secret_dictionary)
                        public_key = key_pair.split("|")[1]
                        outFile.write(public_key)
            return out_path_from_file, "WRITE"

        case "UNIX_SIMPLE_PAYLOAD":
            if not already_exists:
                shutil.copyfile("./static_files/unix_simple_payload.sh", out_path)
            return out_path_from_file, "WRITE"

        case "BUSINESS_FILE_PDF":
            if not already_exists:
                shutil.copyfile("./static_files/business_file.pdf", out_path)
            return out_path_from_file, "WRITE"

        case "STUDENT_FILE_PDF":
            if not already_exists:
                shutil.copyfile("./static_files/student_file.pdf", out_path)
            return out_path_from_file, "WRITE"

        case "SUDOERS_ADD_LESS_TO_ALL":
            if not already_exists:
                with open("./static_files/sudoers_add_less_to_all") as inFile:
                    script = inFile.read()
                with open(out_path, "w+") as outFile:
                    outFile.write(script.format(user_name=user_name))
            return out_path_from_file, "APPEND"

        case "ENABLE_SSH_PASSWORD_CONNECTION":
            if not already_exists:
                shutil.copyfile("./static_files/enable_ssh_password_connection", out_path)
            return out_path_from_file, "EDIT PasswordAuthentication no"

        case "REMOVE_ADDITIONAL_SSH_CONFIG":
            if not already_exists:
                with open(out_path, "w") as out_file:  # it just makes it easier to create an empty file
                    pass
            return out_path_from_file, "DELETE"

        case "CRONTAB_SCAN_SH":
            if not already_exists:
                with open("./static_files/crontab_scan.sh") as inFile:
                    script = inFile.read()
                with open(out_path, "w+") as outFile:
                    outFile.write(script.format(user_name=user_name))
            return out_path_from_file, "APPEND"

        case "SCAN_SH_FOR_CRONTAB":
            if not already_exists:
                shutil.copyfile("./static_files/scan_sh_for_crontab", out_path)
            return out_path_from_file, "WRITE"

        case "SUDO_1.8.2_EXPLOIT":
            if not already_exists:
                with open("./static_files/sudo_1.8.2_exploit") as inFile:
                    script = inFile.read()
                with open(out_path, "w+") as outFile:
                    outFile.write(script.format(user_name=user_name))
            return out_path_from_file, "APPEND"

        case "NPM_WEBSITE_WITH_COMMAND_INJECTION_EASY":
            if not already_exists:
                shutil.copytree("./static_files/meme_img_bank_easy", out_path)
            return out_path_from_file, "WRITE"

        case "TP_WEBSITE":
            if not already_exists:
                possible_outputs = ["./static_files/tp_websites/meme_img_bank_easy",
                                    "./static_files/tp_websites/meme_img_bank_medium",
                                    "./static_files/tp_websites/meme_img_bank_hard"]
                output_picked = rd.randint(0, len(possible_outputs) - 1)
                shutil.copytree(possible_outputs[output_picked], out_path)
            return out_path_from_file, "WRITE"

        case "NAMED_CONF_LOCAL":
            if not already_exists:
                zone_name = file_arguments[0]
                with open("./static_files/dns_named_conf_local") as inFile:
                    script = inFile.read()
                with open(out_path, "w+") as outFile:
                    outFile.write(script.format(zone_name=zone_name))
            return out_path_from_file, "WRITE"

        case "ETC_NETWORK_EMPTY_INTERFACES":
            if not already_exists:
                dns_server_name = file_arguments[0]
                with open(out_path, "w+") as outFile:
                    # the actual editing of the file is done in a script for ip reasons
                    # this empty file is generated for constraints checking reasons
                    pass
                    # outFile.write("nameserver {{ IP_" + dns_server_name + " }}\n")
            return out_path_from_file, "APPEND_BEGINNING_TEMPLATE"

        case "DNS_CLIENT_SCRIPT":
            if not already_exists:
                with open("./static_files/dns_client_script.sh") as inFile:
                    script = inFile.read()
                with open(out_path, "w+") as outFile:
                    outFile.write(script)
            return out_path_from_file, "EXECUTE"

        case "DB.CASINOLIMIT.BZH":
            if not already_exists:
                with open("./static_files/db.casinolimit.bzh") as inFile:
                    script = inFile.read()
                with open(out_path, "w+") as outFile:
                    outFile.write(script.format(machine_ip=f"machine_ip_{file_arguments[0]}"))
            return out_path_from_file, "WRITE_TEMPLATE"

        case "VIRTUAL_CAMERA":
            website_url = "git@gitlab.inria.fr:pirat/breizhctf2024/virtual-camera.git"
            if not already_exists:
                if not os.path.isdir(out_path):
                    Repo.clone_from(website_url, out_path)
                    shutil.rmtree(out_path + "/.git")
            return out_path_from_file, "WRITE"

        case "CAMERA_BASH_ALIASES":
            # ideally should have been done with git but too janky
            if not already_exists:
                with open(out_path, "w+") as outFile:
                    outFile.write('# Take snapshot\n')
                    outFile.write('alias snap="curl -X GET http://0.0.0.0:5000/api/snapshot --output snapshot.jpg"\n\n')
                    outFile.write('# Move up\n')
                    outFile.write('alias up="curl -X POST http://0.0.0.0:5000/api/move/up"\n\n')
                    outFile.write('# Move down\n')
                    outFile.write('alias down="curl -X POST http://0.0.0.0:5000/api/move/down"\n\n')
                    outFile.write('# Move left\n')
                    outFile.write('alias left="curl -X POST http://0.0.0.0:5000/api/move/left"\n\n')
                    outFile.write('# Move right\n')
                    outFile.write('alias right="curl -X POST http://0.0.0.0:5000/api/move/right"\n\n')
            return out_path_from_file, "WRITE"

        case "NPM_WEBSITE_BREIZH_CTF":
            website_url = "git@gitlab.inria.fr:pirat/breizhctf2024/web_site.git"
            if not already_exists:
                if not os.path.isdir(out_path):
                    Repo.clone_from(website_url, out_path)
                    shutil.rmtree(out_path + "/.git")
                # edit .env values
                mail_secret = sec_gen.generate_secret(secret_list[0].name, secret_dictionary)
                password = sec_gen.generate_credential("FIXED_NPM_BREIZH_CTF_PASSWORD")

                def replace_in_file(file_path, search_text, new_text):
                    with fileinput.input(file_path, inplace=True) as file:
                        for line in file:
                            new_line = line.replace(search_text, new_text)
                            print(new_line, end='')

                replace_in_file(out_path + "/.env", "MAIL_PORT=tochange", "MAIL_PORT=25")
                replace_in_file(out_path + "/.env", "MAIL_IP=tochange", f"MAIL_IP=bastion.casinolimit.bzh")
                replace_in_file(out_path + "/.env", "MAIL_USER=tochange", f"MAIL_USER={user_name}")
                replace_in_file(out_path + "/.env", "MAIL_PWD=tochange", f"MAIL_PWD={password}")
                replace_in_file(out_path + "/.env", "MAIL_RCVER=tochange", f"MAIL_RCVER={mail_secret}")  # idk

            return out_path_from_file, "WRITE"

        case "BREIZH_CTF_PYTHON_WEBSITE":
            if not already_exists:
                website_url = "git@gitlab.inria.fr:pirat/breizhctf2024/start-website.git"
                if not already_exists:
                    if not os.path.isdir(out_path):
                        Repo.clone_from(website_url, out_path)
                        shutil.rmtree(out_path + "/.git")
            else:
                shutil.move(out_path, os.path.dirname(out_path) + "/welcome")
            out_path = os.path.dirname(out_path) + "/welcome"
            out_path_from_file = "/" + os.path.basename(out_path)
            out_path_from_file = output_directory_from_file + out_path_from_file
            return out_path_from_file, "WRITE"

        case "TP_WEBSITE_SETUP":
            if not already_exists:
                shutil.copyfile("./static_files/tp_website_setup.sh", out_path)
            return out_path_from_file, "EXECUTE"

        case "NPM_WEBSITE_WITH_COMMAND_INJECTION_MEDIUM":
            if not already_exists:
                shutil.copytree("./static_files/meme_img_bank_medium", out_path)
            return out_path_from_file, "WRITE"

        case "NPM_WEBSITE_WITH_COMMAND_INJECTION_HARD":
            if not already_exists:
                shutil.copytree("./static_files/meme_img_bank_hard", out_path)
            return out_path_from_file, "WRITE"

        case "UNIX_DOCUMENTS":
            if not already_exists:
                shutil.copytree("./static_files/default_user_corpus/Documents", out_path)
            return out_path_from_file, "WRITE"

        case "UNIX_IMPORTANT":
            if not already_exists:
                shutil.copytree("./static_files/default_user_corpus/IMPORTANT", out_path)
            return out_path_from_file, "WRITE"

        case "UNIX_MUSIC":
            if not already_exists:
                shutil.copytree("./static_files/default_user_corpus/Music", out_path)
            return out_path_from_file, "WRITE"

        case "UNIX_PICTURES":
            if not already_exists:
                shutil.copytree("./static_files/default_user_corpus/Pictures", out_path)
            return out_path_from_file, "WRITE"

        case "POSTFIX_VIRTUAL_CONF":
            if not already_exists:
                with open(out_path, "w+") as outFile:
                    outFile.write(f"employee@casinolimit.bzh {user_name}\n")
                    outFile.write(f"admin@casinolimit.bzh {user_name}\n")
            return out_path_from_file, "WRITE"

        case "EMAIL_HELPER_DIRECTORY":
            if not already_exists:
                shutil.copytree("./static_files/email_helper_directory", out_path)
            return out_path_from_file, "WRITE"

        case "IP_HINT_EMAIL_TEMPLATE":
            if not already_exists:
                shutil.copyfile("./static_files/ip_hint.txt", out_path)
            return out_path_from_file, "WRITE_TEMPLATE"

        case "GENERATE_EMAILS":
            # Note: any file in /tmp/ following the pattern *mail*sh will be executed as part of a postfix install
            if not already_exists:
                with open("./static_files/generate_emails.sh") as inFile:
                    script = inFile.read()
                with open(out_path, "w+") as outFile:
                    outFile.write(script.format(
                        username=user_name))
            return out_path_from_file, "WRITE"

        case "S-NAIL_OPTIONS":
            if not already_exists:
                with open(out_path, "w+") as outFile:
                    outFile.write("set emptystart\n")
                    outFile.write("set_folder=Mail\n")
                    outFile.write("set_record=+sent\n")
            return out_path_from_file, "APPEND"

        case "DJANGO_WEBSITE_WITH_DIRECTORY_TRAVERSAL_REWARDING_SSH_KEY":
            if not already_exists:
                shutil.copytree("./static_files/django_website", out_path)
            return out_path_from_file, "WRITE"

        case "DJANGO_WEBSITE_WITH_DIRECTORY_TRAVERSAL_REWARDING_PASSWORD":
            if not already_exists:
                shutil.copytree("./static_files/django_website", out_path)
            return out_path_from_file, "WRITE"

        case "DJANGO_USER_CREATE_SUPERUSER":
            if not already_exists:
                with open(out_path, "w+") as outFile:
                    assert len(secret_list) == 1
                    password = sec_gen.generate_secret(secret_list[0].name, secret_dictionary)
                    outFile.write(f"DJANGO_SUPERUSER_PASSWORD='{password}'\n")
                    outFile.write("DJANGO_SUPERUSER_USERNAME='bob'\n")
                    outFile.write("DJANGO_SUPERUSER_EMAIL='bob@bob.com'\n")
            return out_path_from_file, "WRITE"

        case "DJANGO_SSH_NOTE_KEY":
            if not already_exists:
                with open(out_path, "w+") as outFile:
                    for secret in secret_list:
                        key_pair = sec_gen.generate_secret(secret.name, secret_dictionary)
                        private_key = key_pair.split("|")[0]
                        private_key = private_key + "FOR USER SUPERUSER (top secret!!)"
                        outFile.write("[{\n")
                        outFile.write('  "model": "notes.note",\n')
                        outFile.write('  "pk": 123456,\n')
                        outFile.write('  "fields": {')
                        private_key = private_key.replace("\n", " ")
                        outFile.write(f'    "note_text": "{private_key}",\n')
                        outFile.write('"pub_date": "2023-03-20T12:20:48.381Z"\n')
                        outFile.write('  }\n')
                        outFile.write('}]\n')
            return out_path_from_file, "WRITE"

        case "DJANGO_PASSWORD_NOTE":
            with open(out_path, "w+") as outFile:
                for secret in secret_list:
                    # add the user names as a clue
                    for node in secret.related_nodes:
                        outFile.write(
                            "FOR USER" + node.machine + "\n user: "
                            + node.user + "\n")
                    outFile.write(sec_gen.generate_secret(secret.name, secret_dictionary))
            return out_path_from_file, "WRITE"

        case "DISABLE_DYNAMIC_MOTD_BANNER":
            with open(out_path, "w+") as outFile:
                outFile.write("#session    optional     pam_motd.so  motd=/run/motd.dynamic")
            return out_path_from_file, "EDIT motd=/run/motd.dynamic"

        case "AUDITD_DEFAULT_RULES":
            if not already_exists:
                shutil.copyfile("./static_files/audit.rules", out_path)
            return out_path_from_file, "WRITE"

        case "CASINOLIMIT_BANNER":
            if not already_exists:
                shutil.copyfile("./static_files/banners/casinolimit_banner", out_path)
            return out_path_from_file, "WRITE"

        case "CAMERA_BANNER":
            if not already_exists:
                shutil.copyfile("./static_files/banners/camera_banner", out_path)
            return out_path_from_file, "WRITE"

        case "BASTION_BANNER":
            if not already_exists:
                shutil.copyfile("./static_files/banners/bastion_banner", out_path)
            return out_path_from_file, "WRITE"

        case "START_BANNER":
            if not already_exists:
                shutil.copyfile("./static_files/banners/start_banner", out_path)
            return out_path_from_file, "WRITE"

        case "MACHINE_NAME_BANNER":
            if not already_exists:
                color_code = str(rd.randint(31, 36))
                with open(out_path, "bw+") as outFile:
                    outFile.write(b"[1;")
                    outFile.write(str.encode(color_code + "m"))
                    outFile.write(b"\n")
                    outFile.write(str.encode(art.text2art(machine_name)))
                    outFile.write(b"\n")
                    outFile.write(b"[0m")
            return out_path_from_file, "WRITE"

        case "POSTGRESQL_FLAG":
            if not already_exists:
                with open(out_path, "w+") as outFile:
                    for secret in secret_list:
                        outFile.write(sec_gen.generate_secret(secret.name, secret_dictionary))
            return out_path_from_file, "WRITE"

        case "POSTGRESQL_DATABASE":
            if not already_exists:
                shutil.copyfile("./static_files/meme_DB.sql", out_path)
            return out_path_from_file, "WRITE"

        case "POSTGRESQL_DATABASE_ALLOW_ALL_LISTEN_ADRESSES_POSTGRESQL":
            if not already_exists:
                with open(out_path, "w+") as outFile:
                    outFile.write("listen_addresses = '*'")
            return out_path_from_file, "EDIT listen_addresses"

        case "POSTGRESQL_DATABASE_ALLOW_ALL_LISTEN_ADRESSES_HBA":
            if not already_exists:
                with open(out_path, "w+") as outFile:
                    outFile.write("host all all 0.0.0.0/0 md5")
            return out_path_from_file, "APPEND"

        case "POSTGRESQL_CREATE_MEME_DB_FROM_PASSWORD":
            if not already_exists:
                with open("./static_files/meme_db_setup.sh") as inFile:
                    script = inFile.read()
                with open(out_path, "w+") as outFile:
                    assert len(secret_list) == 1
                    secret = secret_list[0]
                    outFile.write(script.format(password=sec_gen.generate_secret(secret.name, secret_dictionary),
                                                username=user_name))
            return out_path_from_file, "EXECUTE"

        case _:
            raise ValueError(f"Invalid file type: {file_type}")
