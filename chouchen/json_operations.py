"""Utility module to handle json procedure files.

This includes:
    - Checking that they are of the correct format.
    - Adding empty entries for entry/exit constraints if not indicated.
    - Fusing every procedure file together.
"""
import json
import logging
from os import listdir
from os.path import isfile, join, getsize, dirname, splitext

import jsonschema
import jsonschema.exceptions

log = logging.getLogger(__name__)
JSON_FOLDER = dirname(__file__) + "/../json/procedures"
OUTPUT = dirname(__file__) + "/../json/fused_procedures.json"


def validate_json_proc(proc_json: dict) -> bool:
    """Checks whether a given json procedure file is of the correct format.
    This is done my comparing it to the schema described in ../json/proc_schema.json .

    Args:
        proc_json: the procedure to check

    Returns:
        True if the procedure is correctly written , False otherwise.
    """
    with open(f"../json/proc_schema.json", "r") as inFile:
        scenario_schema = json.load(inFile)
        inFile.close()
    try:
        jsonschema.validate(instance=proc_json, schema=scenario_schema)
    except jsonschema.exceptions.ValidationError as err:
        log.debug(err)
        log.error(f"""            
            Error while looking for required {err.validator_value} inside the procedure.
            {err.message} :
            {json.dumps(err.instance, indent=4)}""")
        return False
    return True


def add_empty_entry_exit_constraints(json_dic: dict) -> dict:
    """ Rewrites the json fused_procedures file to add all the empty entries.
    Useful for procedures that have no entry or exit constraints.

    Args:
        json_dic: the dictionary to edit.

    Returns:
        The edited version of this dictionary.
    """
    # For every procedure check whether the procedure has an exit or an entry component.
    for tech in json_dic:
        for proc in json_dic[tech]:
            if not ("Entry" in proc):
                proc["Entry"] = EMPTY_CONSTRAINT
            if not ("Exit" in proc):
                proc["Exit"] = EMPTY_CONSTRAINT
    return json_dic


def fuse_json_proc_files() -> None:
    """Fuses every file contained in JSON_FOLDER into a single procedure file.
    Procedures are written in different files for ease of use reasons, but need to be combined for the refinement
    algorithm to work.

    """
    logging.info("Fusing json files...")
    final_dic = {}
    onlyfiles = [f for f in listdir(JSON_FOLDER) if isfile(join(JSON_FOLDER, f))]
    for file in onlyfiles:
        technique_name = file[:-5]
        if getsize(join(JSON_FOLDER, file)) > 0:
            with open(join(JSON_FOLDER, file), "r") as inFile:
                jsonfile = json.load(inFile)
                final_dic[technique_name] = jsonfile
        else:
            logging.info(f"File {file} is empty, skipping...")
    final_dic = add_empty_entry_exit_constraints(final_dic)
    with open(OUTPUT, "w+") as outFile:
        json.dump(final_dic, outFile)


def get_all_available_technique_names():
    file_names = [f for f in listdir(JSON_FOLDER) if isfile(join(JSON_FOLDER, f))]
    tech_names = [splitext(file)[0] for file in file_names]
    return [x + ": " + get_technique_name(x) for x in tech_names]


EMPTY_CONSTRAINT = {
    "OS": [],
    "Account": [],
    "Software": [],
    "Files": []
}


def get_technique_name(tech: str) -> str:
    """Converts a technique number (written as TXXXX) into the full technique name.
    The full list of technique names is stored in ../json/technique_name_map.json .
    Example: for tech = "T1002", this will return "Data Compressed"

    Args:
        tech: The technique number, written as TXXXX.

    Returns: A string corresponding to the given technique name.
    Raises:
        FileNotFoundError: the technique_name_map.json file is missing from the json directory.
    """
    with open("./json/technique_name_map.json", "r") as inFile:
        technique_dictionary = json.load(inFile)
        return technique_dictionary[tech]
