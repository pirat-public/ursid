"""
Procedural refinement module.
Responsible for converting a scenario described on a technical level into a scenario described on a procedural level.
This is done by, in order:
    - Get the list of all available procedures (as written in ./json/procedures.
    - Apply a backtracking algorithm in order to pick procedures for each technique, and checking for constraint
    inconsistencies.
    - Generate the appropriate secrets and files.
    - Converts the resulting scenario object into a json file.

"""

import copy
import json
import logging
import random as rd
from typing import Tuple

import chouchen.json_operations as json_operations
import chouchen.secret_generation as secg
import chouchen.subconstraints
import chouchen.system_graph as sg
from chouchen.constraints import ArchitecturalConstraints, CombiningIncompatibleConstraintsError, SecretPreconditions, \
    AccountConstraints
from chouchen.subconstraints import FilePermission, AccountName, OSType

log = logging.getLogger(__name__)


def get_compatible_procedures(transition: sg.Transition, proc_dictionary: dict, secret_dictionary: dict) -> list:
    """Gets a list of all procedures eligible for a transition according to their secret preconditions.

    Args:
        transition (sg.Transition): the relevant transition
        proc_dictionary(dict): the dictionary containing all procedure constraints
        secret_dictionary(dict): the dictionary containing all information related to already generated secrets

    Returns:
        the list of all procedures eligible for this transition.

    """
    # First you get all the procedures for the technique
    possible_procs = proc_dictionary[transition.technique]
    result = []
    for proc in possible_procs:
        requires_condition = proc["Secret Preconditions"]["Requires"]
        rewards_condition = proc["Secret Preconditions"]["Rewards"]
        secret_preconditions = SecretPreconditions(requires_condition, rewards_condition)
        if secret_preconditions.is_eligible(transition, secret_dictionary):
            result.append(proc)
    return result


def sort_transitions(transition_list: list) -> None:
    """Sorts a list of transitions to put the ones that already have assigned procedures first.
    Then it puts transitions that require or reward secrets.
    This sort is done in-place.

    Args:
        transition_list: The list of transitions to sort.

    """
    index = 0
    for i in range(len(transition_list)):
        transition = transition_list[i]
        if transition.procedure:
            temp = transition_list[index]
            transition_list[index] = transition
            transition_list[i] = temp
            index += 1
    for i in range(index, len(transition_list)):
        transition = transition_list[i]
        if transition.requires or transition.rewards:
            temp = transition_list[index]
            transition_list[index] = transition
            transition_list[i] = temp
            index += 1


def initialize_constraints(s_tech: sg.SystemGraph) -> dict:
    """ Walks the scenario and returns an empty configuration file with entries for each machine and user contained
    within the scenario.

    Args:
        s_tech(sg.SystemGraph): A scenario defined on a procedural level.

    Returns:
        A dictionary of architecture constraint objects.
    """

    # First get list of all machines in the scenario

    for node in s_tech.nodes:
        if not s_tech.additional_config:
            if "vms" not in s_tech.additional_config:
                s_tech.additional_config["vms"] = {}
            s_tech.additional_config["vms"][node.machine] = {}  # Empty additional config, avoids errors laters
        elif node.machine not in s_tech.additional_config["vms"]:
            s_tech.additional_config["vms"][node.machine] = {}

    # Goes through the list of nodes.
    output_dictionary = {}
    for node in s_tech.nodes:
        if node.machine not in output_dictionary:
            # EDIT HERE IF YOU WANNA CHANGE FROM LINUX
            # output_dictionary[node.machine] = ArchitecturalConstraints.empty_constraint_with_account(node.user)
            output_dictionary[node.machine] = \
                ArchitecturalConstraints.default_unix_template_with_account(node.machine, node.user,
                                                                            s_tech.additional_config["vms"][
                                                                                node.machine])
        elif not output_dictionary[node.machine].is_user_in_constraint(node.user):
            new_constraint = AccountConstraints(node.user)
            output_dictionary[node.machine].account_constraints.append(new_constraint)
    return output_dictionary


def edit_procedure_constants(procedure: dict, transition: sg.Transition) -> dict:
    """Returns a procedure dictionary where some of its entries have been edited to better fit the scenario.
    For now, this only changes the values of "EXIT_USER" and "ENTRY_USER" in file permission constraints to match
    the ones in the scenario.
    Note: This might be doable without copying everything, but the performance changes should be negligible?
    Args:
        procedure: The procedure to alter
        transition: The transition currently being refined.

    Returns:
        A new procedure dictionary with the corresponding entries edited.
    """

    new_proc = copy.deepcopy(procedure)

    def edit_entry_or_exit(machine: str) -> None:  # machine should be either Entry or Exit
        """Edits dynamic entries such as "ENTRY_USER" or "EXIT_USER" in a procedure to fit the scenario.

        Args:
            machine: "Entry" or "Exit". Used to avoid copy pasting code.
        """
        for file in new_proc[machine]["Files"]:
            perms = file["Permissions"]
            if perms["User"] == FilePermission.EntryUser:
                perms["User"] = transition.entry_node.user
            if perms["User"] == FilePermission.ExitUser:
                perms["User"] = transition.exit_node.user
            if perms["Group"] == FilePermission.EntryUser:
                perms["Group"] = transition.entry_node.user
            if perms["Group"] == FilePermission.ExitUser:
                perms["Group"] = transition.exit_node.user

        for account in new_proc[machine]["Account"]:
            if account["Name"] == AccountName.EntryUser:
                account["Name"] = transition.entry_node.user
            if account["Name"] == AccountName.ExitUser:
                account["Name"] = transition.exit_node.user
            if account["Group"] == AccountName.EntryUser:
                account["Group"] = transition.entry_node.user
            if account["Group"] == AccountName.ExitUser:
                account["Group"] = transition.exit_node.user

    edit_entry_or_exit("Entry")
    edit_entry_or_exit("Exit")
    return new_proc


def refinement_backtracking(scenario: sg.SystemGraph, transitions_left: list[sg.Transition], proc_dic: dict,
                            arch_constraints: dict[ArchitecturalConstraints],
                            secret_dictionary: dict) -> Tuple[bool, dict, sg.SystemGraph, dict]:
    """A backtracking refinement algorithm for a given scenario. At the end of the function, all transitions in the
    scenario object will have a procedure assigned to them (represented by its name), and the resulting architectural
    constraints will be returned.
    # TODO: Rewrite this more elegantly (weird to output 3 different objects of 3 different types)
    Args:
        scenario (sg.SystemGraph): The scenario to refine.
        transitions_left (list[sg.Transition]): The list of transitions left to refine.
        proc_dic (dict): The dictionary containing procedures for every technique.
        arch_constraints (dict(ArchitecturalConstraints)): The current dictionary of architectural constraints.
        secret_dictionary(dict): The current dictionnary of all generated secrets
    Returns:
        is_valid (bool): Indicates whether the backtracking found a solution or not.
        final_arch_constarints (dict): The resulting architecture constraints. Will be None if is_valid is False.
        new_scenario (sg.SystemGraph): The refined scenario - all the transitions have been given a procedure.
        secret_dictionary (dict): the updated secret dictionary.
    """

    if len(transitions_left) == 0:
        return True, arch_constraints, scenario, secret_dictionary
    else:
        sort_transitions(transitions_left)
        transition = transitions_left[0]
        log.debug(f"Refining transition {transition}...")
        if len(proc_dic[transition.technique]) == 0:
            log.warning("No valid procedures left, backtracking...")
            return False, None, scenario, secret_dictionary
        if transition.procedure:  # Case where procedures have been pre selected
            logging.info(f"""Forced usage of procedure {transition.procedure} on transition {transition}""")
            compatible_procs = ArchitecturalConstraints.get_list_from_procedure_pattern(transition)
        else:
            compatible_procs = get_compatible_procedures(transition, proc_dic, secret_dictionary)
        rd.shuffle(compatible_procs)
        for procedure in compatible_procs:
            log.debug(f"Trying procedure {procedure['Name']}...")
            # Edit some of the procedure values (such as replacing ENTRY_USER and EXIT_USER by the correct values)
            # Useful for file permission constraints
            procedure = edit_procedure_constants(procedure, transition)
            new_arch_constraints = copy.deepcopy(arch_constraints)
            new_secret_dictionary = copy.deepcopy(secret_dictionary)
            entry_machine = transition.entry_node.machine
            exit_machine = transition.exit_node.machine
            try:
                proc_entry_constraint = ArchitecturalConstraints(procedure["Entry"], transition,
                                                                 transition.entry_node.machine,
                                                                 transition.entry_node.user)
                proc_exit_constraint = ArchitecturalConstraints(procedure["Exit"], transition,
                                                                transition.exit_node.machine, transition.exit_node.user)
                proc_entry_constraint.assign_secrets_to_subconstraints()
                proc_exit_constraint.assign_secrets_to_subconstraints()
                proc_entry_constraint.edit_special_cases(scenario)
                proc_exit_constraint.edit_special_cases(scenario)
                new_arch_constraints[entry_machine] = \
                    new_arch_constraints[entry_machine].combine_constraints(proc_entry_constraint)
                new_arch_constraints[exit_machine] = \
                    new_arch_constraints[exit_machine].combine_constraints(proc_exit_constraint)
                new_transitions_left = transitions_left[1:]
                new_scenario = copy.deepcopy(scenario)
                # Get the required secrets from the transition
                secret_preconditions = SecretPreconditions(procedure["Secret Preconditions"]["Requires"],
                                                           procedure["Secret Preconditions"]["Rewards"])
                secret_preconditions.update_secret_dictionary_for_transition(new_secret_dictionary,
                                                                             transition, scenario)

                # add the procedure to the corresponding transition in the new scenario
                new_scenario.transitions[new_scenario.find_transition_index(transition)].procedure = procedure["Name"]
                is_valid, final_arch_constraints, new_scenario, new_secret_dictionary \
                    = refinement_backtracking(new_scenario, new_transitions_left, proc_dic, new_arch_constraints,
                                              new_secret_dictionary)
                if is_valid:
                    return True, final_arch_constraints, new_scenario, new_secret_dictionary
            except CombiningIncompatibleConstraintsError:
                log.debug("Incompatible constraints!")
        log.warning("No valid procedures left, backtracking...")
        return False, arch_constraints, scenario, secret_dictionary


def procedural_refinement(scenario: sg.SystemGraph) -> Tuple[dict, sg.SystemGraph, dict]:
    """Performs the procedural refinement of the given entry scenario.
    This function initializes an empty set of constraints for each machine contained within the scenario and an
    empty secret dictionary, then calls the backtracking refinement algorithm.

    Args:
        scenario(sg.SystemGraph): The system graph object to refine.

    Returns:
        final_constraints(dict): The set of constraints resulting from the backtracking algorithm.
        new_scenario(sg.SystemGraph): The refined scenario - all the transitions have been given a procedure.
        secret_dictionary (dict): The updated secret dictionary.
    """
    # TODO: add ways of setting possible deployable OS
    # This would be done by adding fake transitions with procedures only containing OS constraints
    transitions_left = scenario.transitions
    proc_list = json.load(open(json_operations.OUTPUT))
    arch_constraints = initialize_constraints(scenario)
    secret_dictionary = {}
    if scenario.additional_config:
        if "vms" in scenario.additional_config:
            for machine_name, machine_config in scenario.additional_config["vms"].items():
                if "secrets" in machine_config:
                    for secret_name in machine_config["secrets"]:
                        secret_dictionary[secret_name] = secg.Secret(secret_name,
                                                                     chouchen.subconstraints.SecretType(
                                                                         "PLAINTEXT_PASSWORD"),
                                                                     machine_config["secrets"][secret_name])
    # Calling the backtracking algorithm
    success, final_constraints, new_scenario, secret_dictionary = \
        refinement_backtracking(scenario, transitions_left, proc_list, arch_constraints, secret_dictionary)
    if not success:
        # TODO: proper error handling
        log.error("Scenario was imposible to refine, exiting...")
        raise ValueError
    log.info("Scenario successfully refined!")
    return final_constraints, new_scenario, secret_dictionary


def generate_config_file(arch_constraints: dict, secret_dictionary: dict,
                         output_path: str, additional_config: dict) -> None:
    """Generates the configuration file and writes it at output_path.
    This file is generated using the ArchitecturalConstraints described in arch_constraint and the secret_dictionary.
    A few additional steps to clean the constraints are performed:
        - Any node named "Internet" is removed.
        - Some constraints are fused (like 2 constraints indicating linux at any version, or constraints related
        to the same user)
        - OS and software versions are converted into readable stirngs.
        - Wildcards (such as user groups or credentials) are set to default values.

    Args:
        arch_constraints (dict): the constraints resulting from the refinement process.
        secret_dictionary (dict): the secret dictionary
        output_path (str): the path where the configuration file will be written in.
        additional_config (dict): option lists for all machines (configured in .yml)

    Returns:

    """
    final_list = []
    # Remove "Internet" machine
    if "internet" in arch_constraints:
        del arch_constraints["internet"]
    if "attacker_machine" in arch_constraints:
        del arch_constraints["attacker_machine"]
    if "attacker" in arch_constraints:
        del arch_constraints["attacker"]
    for machine in arch_constraints:
        arch_constraints[machine].generate_credentials_and_files(secret_dictionary)
        arch_constraints[machine].fuse_duplicates()
        final_list.append(arch_constraints[machine].to_dic(machine, secret_dictionary))

    for machine in final_list:
        for os in machine["OS"]:
            if os["Type"] in OSType.OS_IMPLICATIONS:
                os["Type"] = rd.choice(OSType.OS_IMPLICATIONS[os["Type"]])
            if os["Version"] == "+inf":
                # pick a random version for the corresponding OS
                os["Version"] = rd.choice(OSType.OS_VERSIONS[os["Type"]])

        for software in machine["Software"]:
            if software["Version"] == "+inf":
                software["Version"] = "*"
            if software["Port"] == "-1":
                del software["Port"]
            # Specific cases: some software have version numbers as "X.Y.Z" but are converted to floats for computation
            # reasons
            special_case_software_list = ["sudo_project:sudo", "python"]
            if software["Type"] in special_case_software_list:
                if software["Version"] != "*":
                    # convert it back to X.Y.Z format
                    temp = software["Version"].split(".")  # converts 1.82 to [1,82]
                    software["Version"] = temp[0] + "." + temp[1][0] + "." + temp[1][1:]
                    # add a 0 if ends with a dot
                    if software["Version"][-1] == ".":
                        software["Version"] = software["Version"] + "0"

            if software["Type"] == "linux:kernel":  # cheating coz idk how to do it
                # we assume it's something like 5.15.0-57
                # ideally we'd have a text list of every valid linux kernel version to check but i haven't found one
                if software["Version"] != str(5.15025):
                    log.warning("Careful: Using a kernel version which might be unsuported - make sure to "
                                "check your output")
                temp = software["Version"].split(".")
                software["Version"] = temp[0] + "." + temp[1][0:2] + "." + temp[1][2] + "-" + temp[1][3:5]

        # Some software has to be installed before other, so we reorder the list around
        priority_software_list = ["gnu:gcc", "canonical:libpam0g-dev", "gnu:make"]
        new_software_list = []
        for software in machine["Software"]:
            if software["Type"] in priority_software_list:
                new_software_list.append(software)
        for software in machine["Software"]:
            if software["Type"] not in priority_software_list:
                new_software_list.append(software)
        machine["Software"] = new_software_list

        # for now OS entries should always be of size 1
        assert len(machine["OS"]) == 1
        machine["OS"] = machine["OS"][0]
        for user in machine["Users"]:
            # set default group (same as user name)
            if user["Group"] == "*":
                user["Group"] = user["Name"]
            # set default privilege (User)
            if user["Privilege"] == "*":
                user["Privilege"] = "User"
        # For every file, check their Modification status
        # if it's "EXECUTE" move the file to Inline Files
        machine["Inline Files"] = [file for file in machine["Files"] if file["Modification"] == "EXECUTE"]
        machine["Files"][:] = [file for file in machine["Files"] if file["Modification"] != "EXECUTE"]

        # Network names. If empty, apiculteur will fill it with default values
        # For a range 10.35.X.1, first deployment will be 10.35.1.1, then 10.35.2.1...
        if "global_network_pattern" in additional_config:
            machine["Global Network Pattern"] = additional_config["global_network_pattern"]
        if "vms" in additional_config:
            if machine["Name"] in additional_config["vms"]:
                if "dns_server" in additional_config["vms"][machine["Name"]]:
                    machine["DNS Server IP"] = additional_config["vms"][machine["Name"]]["dns_server"]
                if "networks" in additional_config["vms"][machine["Name"]]:
                    final_networks = []
                    for network in additional_config["vms"][machine["Name"]]["networks"]:
                        network_config = network.split(" ")
                        if len(network_config) != 1:
                            raise ValueError("Invalid config")
                        else:
                            network_name = network_config[0]
                        final_networks.append(network_name)
                    machine["Networks"] = final_networks

    with open(output_path, "w+") as outFile:
        json.dump(final_list, outFile, indent=2)
