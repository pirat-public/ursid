"""Main module. Combines every other module together in order to combine a given scenario into several different
architectural descriptions.
This module can also be used to describe scenarios using python classes and functions (see function main_scenario())
and export them as a json file in order to be imported later.


"""
import argparse
import logging
import os
import shutil

import yaml

import apiculteur.main
import chouchen.graphics as graphics
import chouchen.json_operations as json_operations
import chouchen.procedural_refinement as pr
import chouchen.system_graph as sg
import utils.log
from utils import user_confirm

project_name = "URSID"


def main_scenario():
    return cerbere_scenario()


easy_website = "Exploit Public-Facing Application: Website with command injection (easy)"
medium_website = "Exploit Public-Facing Application: Website with command injection (medium)"

vulnerable_sudo = "Exploitation for privilege escalation: Vulnerable sudo version (CVE-2019-14287)"
vulnerable_pkexec = "Exploitation for privilege escalation: Vulnerable pkexec process"

txt_credentials = "Unsecured Credentials: Passwords in txt file"
bash_history_credentials = "Unsecured Credentials: Passwords in .bash_history"

# It says "debien" on all of them but I'm tired of rewriting scenarios and that one doesn't have a SIGMA rule anyway.
debian_website = "Exploit Public-Facing Application: Django directory traversal rewarding ssh key (debian)"
ubuntu_website = "Exploit Public-Facing Application: Django directory traversal rewarding ssh key (ubuntu)"

remote_services_ssh = "Remote Services: SSH Access from key"
remote_services_sql = "Remote Services: SQL server rewarding a flag"


def apt_3_scenario() -> sg.SystemGraph:
    """Creates a corresponding to the CERBERE scenario.
    # TODO: Either delete or make sure it still works.

    Returns:
        A SystemGraph object corresponding to a scenario based on the modus operandi of APT 29.
    """
    n1 = sg.Node("Attacker", "SuperUser")
    n2 = sg.Node("Bear", "Alice")
    n3 = sg.Node("Bear", "SuperUser")
    n4 = sg.Node("Raccoon", "Bob")
    n5 = sg.Node("Raccoon", "SuperUser")
    n6 = sg.Node("Badger", "Charlie")
    n7 = sg.Node("Badger", "SuperUser")
    n8 = sg.Node("Skunk", "Diana")
    n9 = sg.Node("Skunk", "SuperUser")

    t1 = sg.Transition(n1, n2, "T1566", procedure="Phishing: Linux Payload")
    t2 = sg.Transition(n2, n2, "T1005", procedure="Data From Local System: Student File Corpus")
    t3 = sg.Transition(n2, n3, "T1548", procedure="Abuse Elevation Control Mechanism: Bad Sudo Config")
    t4 = sg.Transition(n3, n3, "T1003", procedure="OS Credential Dumping: Crackable password in /etc/shadow",
                       rewards=["raccoon_secret", "badger_secret"])  # OS Credential Dumping
    t5 = sg.Transition(n3, n3, "T1552", procedure="Unsecured Credentials: Private key in .ssh",
                       rewards=["skunk_secret"])  # Unsecured Credentials

    t7 = sg.Transition(n3, n4, "T1021", procedure="Remote Services: SSH Access from password",
                       requires=["raccoon_secret"])  # Remote Services (Valid Accounts)
    t8 = sg.Transition(n4, n4, "T1005", procedure="Data From Local System: Business File Corpus")
    t9 = sg.Transition(n4, n5, "T1068", procedure="Exploitation for Privilege Escalation: Vulnerable sudo version")
    t10 = sg.Transition(n5, n5, "T1552", rewards=["skunk_su_secret"],
                        procedure="Unsecured Credentials: Passwords in text file")

    t11 = sg.Transition(n3, n6, "T1021", procedure="Remote Services: SSH Access from password",
                        requires=["badger_secret"])
    t13 = sg.Transition(n6, n6, "T1005", procedure="Data From Local System: Business File Corpus")
    t14 = sg.Transition(n6, n7, "T1548", procedure="Abuse Elevation Control Mechanism: Bad Sudo Config")

    t15 = sg.Transition(n3, n8, "T1021", requires=["skunk_secret"], procedure="Remote Services: SSH Access from key")
    t16 = sg.Transition(n8, n8, "T1005", procedure="Data From Local System: Student File Corpus")
    t17 = sg.Transition(n8, n9, "T1078", procedure="Valid Accounts: Password from secret", requires=["skunk_su_secret"])
    t18 = sg.Transition(n8, n9, "T1053", procedure="Scheduled Task/Job: Cronjob with bad permissions")

    starting = [n1]
    victory = [n9]
    nodes = [n1, n2, n3, n4, n5, n6, n7, n8, n9]
    transitions = [t1, t2, t3, t4, t5, t7, t8, t9, t10, t11, t13, t14, t15, t16, t17, t18]
    sys_graph = sg.SystemGraph(nodes, starting, victory, transitions)
    attack_state = sg.AttackState([n1, n2, n3, n4, n5, n8, n9], [t1, t3, t5, t7, t10, t9, t15, t17],
                                  ["skunk_secret", "skunk_su_secret"])
    return sys_graph


def cerbere_scenario() -> sg.SystemGraph:
    """Creates a corresponding to the CERBERE scenario.
    If you wish to edit this scenario, you may then regenerate the corresponding json using the "generate_json"
    argument in the command line.
    Returns:
        A SystemGraph object corresponding to the scenario as played in the CERBERE experiment.
    """
    n1 = sg.Node("Attacker", "SuperUser")
    n2 = sg.Node("Zagreus", "Alice")
    n3 = sg.Node("Zagreus", "SuperUser")
    n4 = sg.Node("Hades", "Bob")
    n5 = sg.Node("Hades", "SuperUser")
    n6 = sg.Node("Melinoe", "postgres")
    t1 = sg.Transition(n1, n2, "T1190", procedure=medium_website)
    t2 = sg.Transition(n2, n3, "T1068", procedure=vulnerable_pkexec)
    t3 = sg.Transition(n3, n3, "T1552", rewards=["zagreus_secret"], procedure=bash_history_credentials)
    t4 = sg.Transition(n1, n4, "T1190", requires=["zagreus_secret"], rewards=["hades_secret"], procedure=debian_website)
    t5 = sg.Transition(n1, n5, "T1021", requires=["hades_secret"], procedure=remote_services_ssh)
    t6 = sg.Transition(n5, n5, "T1552", rewards=["melinoe_db_password"], procedure=txt_credentials)
    t7 = sg.Transition(n5, n6, "T1021", requires=["melinoe_db_password"], rewards=["melinoe_flag"],
                       procedure=remote_services_sql)
    nodes = [n1, n2, n3, n4, n5, n6]
    transitions = [t1, t2, t3, t4, t5, t6, t7]
    starting = [n1]
    victory = [n6]
    sys_graph = sg.SystemGraph(nodes, starting, victory, transitions)
    return sys_graph


def parse_arguments() -> argparse.Namespace:
    """Handles parsing args.

    Returns:
        A parser object containing every argument.
    """
    parser = argparse.ArgumentParser(description='Refine a unique scenario description '
                                                 'into several different architectures.')
    parser.add_argument(
        "scenario_path",
        type=str,
        help="Path of the scenario to refine."
    )
    parser.add_argument(
        "-n",
        "--number",
        default=1,
        type=int,
        help="The number of procedural scenarios that will be generated.",
        nargs="?"
    )
    parser.add_argument(
        "--generate_scenario_json",
        action="store_true",
        help="Converts the scenario described in the main_scenario() function in main.py into a "
             "json file to the given path."
    )

    parser.add_argument(
        "--refinement_only",
        action="store_true",
        help="Only performs the refinement process, not vagrant/ansible files will be generated."
    )

    parser.add_argument(
        "--output_directory",
        default="./output",
        help="Specify output directory"
    )

    parser.add_argument(
        "-v",
        "--verbose",
        action="count",
        help="Increase output verbosity",
    )

    parser.add_argument(
        "--ignore_virtualbox",
        action="store_true",
        help="Ignore any virtualbox commands: no Vagrantfile creation, no check for vms, no networks created. "
             "Only use if you know what you're doing.",
    )

    parser.add_argument(
        "-nn",
        "--no-nat",
        action="store_true",
        help="NAT interface will be removed after the vms generation.",
    )

    parser.add_argument(
        "-aie",
        "--ansible-ignore-errors",
        action="store_true",
        help="Ansible will finish providing even if some tasks throw an error.",
    )
    parser.add_argument(
        "-ado",
        "--auto_delete_output",
        action="store_true",
        help="Do not ask for user confirmation to delete existing output directory"
    )

    return parser.parse_args()


def main(arguments: argparse.Namespace) -> None:
    """Main function. Refines the scenario n times and creates appropriates folders for each of them in the "./output/"
    folder.
    Each scenario will be in its own subfolder (scenario_0, scenario_1...) which will contain:
        - A .json describing the scenario on a procedural level.
        - A folder named "generated_files" containing every file necessary for the scenario.
        - A .png showing the procedural graph.

    Note that the output folder will be created if it doesn't exist. If it does exist, user will be asked whether they
    want to delete its content.
    Args:
        arguments: The list of arguments as parsed.
    """
    if args.generate_scenario_json:
        scenario = main_scenario()
        sg.export_scenario_as_json(scenario, args.scenario_path)
        log.info(f"Successfully exported scenario in {args.scenario_path}!")
        exit(1)

    json_operations.fuse_json_proc_files()
    # create output folder if it doesn't exist yet
    args.output_directory = os.path.abspath(args.output_directory)
    folder = args.output_directory
    if not os.path.isdir(folder):
        logging.info("Creating output folder...")
        os.mkdir(folder)
    # clear all previous output
    if len(os.listdir(folder)) != 0:
        if not args.auto_delete_output:
            ask_confirm = utils.user_confirm.user_confirmation(
                f"{len(os.listdir(folder))} files or folders detected in output directory! "
                f"Would you like to delete those before continuing? The project will probably fail if this directory "
                f"is not empty.")

        for filename in os.listdir(folder):
            file_path = os.path.join(folder, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                print('Failed to delete %s. Reason: %s' % (file_path, e))

    scenario = sg.import_scenario_from_json(args.scenario_path)
    beatable, transitions = scenario.is_winnable()
    if not beatable:
        log.warning(
            "Scenario is not beatable on a technical level. Generation is possible but may not lead to a satisfying architecture.")
    expected_yml_path = os.path.splitext(args.scenario_path)[0] + ".yml"
    if os.path.exists(expected_yml_path):
        log.info(f"Loaded additional config from {expected_yml_path}")
        with open(expected_yml_path, "r") as in_file:
            scenario.additional_config = yaml.safe_load(in_file)
    for i in range(arguments.number):
        # if output/scenario folder doesn't exist create it
        if not os.path.isdir(f"{folder}/scenario"):
            os.mkdir(f"{folder}/scenario")
        # same for generated files
        if not os.path.isdir(f"{folder}/scenario/generated_files"):
            os.mkdir(f"{folder}/scenario/generated_files")
        final_constraints, new_scenario, secret_dictionary = pr.procedural_refinement(scenario)
        output_path = f"{folder}/scenario" + "/output_scenario.json"
        output_graph_path = f"{folder}/scenario" + "/output_scenario"
        pr.generate_config_file(final_constraints, secret_dictionary, output_path, scenario.additional_config)
        graphics.draw_graph(new_scenario, proc_mode=True, save_mode=output_graph_path)
        # rename folder
        os.rename(f"{folder}/scenario/", "./output/scenario_" + str(i) + "/")
        logging.info(f"Scenario generated in ./output/scenario_" + str(i) + "/ !")
        logging.info(f"Difficulty: {sg.get_scenario_difficulty(new_scenario)}")
    # Convert .json into Vagrant and Ansible files
    if not args.refinement_only:
        log.info(f"Converting files into Vagrant and Ansible...")
        apiculteur.main.build_vagrant_ansible(args)


if __name__ == "__main__":
    args = parse_arguments()
    utils.log.init_logging(args)
    log = logging.getLogger(project_name)
    main(args)
