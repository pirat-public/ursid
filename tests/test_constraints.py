"""Tests for constraints and subconstraints.
"""
import unittest
import chouchen.constraints as constraints
import chouchen.subconstraints as subconstraints
import chouchen.secret_generation as secret_generation

import chouchen.system_graph as sg

# TODO: EDIT all procedures, rn tests are broken because they need to be rewritten.


class TestSubconstraints(unittest.TestCase):
    def test_os_subconstraint_compatibility(self) -> None:
        # Test OSType subconstraints
        ubuntu = subconstraints.OSType("ubuntu")
        debian = subconstraints.OSType("debian")
        wildcard = subconstraints.OSType("*")
        # Debian should not be compatible with Ubuntu
        assert not (ubuntu.is_compatible(debian))
        # Ubuntu should be compatible with "*"
        assert ubuntu.is_compatible(wildcard)

        # Test Version subconstraints
        equal_5 = subconstraints.OSVersion("==5")
        less_4 = subconstraints.OSVersion("<=4")
        more_3 = subconstraints.OSVersion(">=3")
        empty = subconstraints.OSVersion("*")
        # 5 should not be compatible with <= 4
        assert not (equal_5.is_compatible(less_4))
        # 5 should be compatible with >=3 
        assert equal_5.is_compatible(more_3)
        # <=4 should be compatible with >=3
        assert less_4.is_compatible(more_3)
        # <=4 should be compatible with itself
        assert less_4.is_compatible(less_4)
        # <=4 should be compatible with *
        assert less_4.is_compatible(empty)

        osc1 = constraints.OSConstraints("ubuntu", "==5")
        osc2 = constraints.OSConstraints("debian", "==5")
        # This should fail because ubuntu and debian are not compatible
        assert not (osc1.is_compatible(osc2))

        osc3 = constraints.OSConstraints("ubuntu", "==5")
        osc4 = constraints.OSConstraints("ubuntu", ">=3")
        # This should succeed because both version and type are compatible.
        assert (osc3.is_compatible(osc4))

    def test_software_constraints(self) -> None:
        # Test software name subconstraints
        ssh = subconstraints.SoftwareType("ssh")
        sudo = subconstraints.SoftwareType("sudo")
        assert ssh != sudo
        assert ssh == ssh

        # Test software version subconstraints
        ver1 = subconstraints.SoftwareVersion("==1.9")
        ver2 = subconstraints.SoftwareVersion("<=1.95")
        assert ver1.is_compatible(ver2)

        # Test software port subconstraints
        port1 = subconstraints.SoftwarePort(22)
        port2 = subconstraints.SoftwarePort()
        assert port1 != port2
        assert port1 == port1

        # Test two software constraints with same type
        soft1 = constraints.SoftwareConstraints("ssh", "==1.9", 22)
        soft2 = constraints.SoftwareConstraints("ssh", "<=1.95", 22)
        assert soft1.is_compatible(soft2)

        # Test two software constraints with different types
        soft3 = constraints.SoftwareConstraints("sudo", "==1.9")
        soft4 = constraints.SoftwareConstraints("sudo", "==1.9", 22)
        assert soft3.is_compatible(soft1)
        assert not soft4.is_compatible(soft1)

    def test_account_constraints(self) -> None:
        # Test name subconstraints
        alice = subconstraints.AccountName("Alice")
        bob = subconstraints.AccountName("Bob")
        assert alice != bob

        # Test group subconstraints
        charlie = subconstraints.AccountGroup("Charlie")
        root = subconstraints.AccountGroup("root")
        assert not charlie.is_compatible(root)

        # Test permission subconstraints
        user = subconstraints.AccountPrivilege(subconstraints.AccountPrivilege.User)
        superuser = subconstraints.AccountPrivilege(subconstraints.AccountPrivilege.SuperUser)
        assert not user.is_compatible(superuser)
        assert user.is_compatible(user)

        # Test credentials subconstraints
        weak_pw = subconstraints.AccountCredentials(subconstraints.AccountCredentials.StrongRandomPassword)
        strong_pw = subconstraints.AccountCredentials(subconstraints.AccountCredentials.WeakRandomPassword)
        assert not weak_pw.is_compatible(strong_pw)
        assert weak_pw.is_compatible(weak_pw)
        # Test 2 accounts with different names

        weak_pw_type = subconstraints.AccountCredentials.weak_random_password()
        strong_pw_type = subconstraints.AccountCredentials.strong_random_password()

        account_1 = constraints.AccountConstraints("Alice", "Alice", "User", strong_pw_type)
        account_2 = constraints.AccountConstraints("Bob", "Alice", "User", weak_pw_type)
        assert account_1.is_compatible(account_2)
        # Test 2 accounts with the same name
        account_3 = constraints.AccountConstraints("Alice", "Bob", "User", strong_pw_type)
        assert not account_3.is_compatible(account_1)
        account_4 = constraints.AccountConstraints("Alice", "Alice", "SuperUser", strong_pw_type)
        assert not account_4.is_compatible(account_1)
        account_5 = constraints.AccountConstraints("Alice", "Alice", "User", weak_pw_type)
        assert not account_5.is_compatible(account_1)

    def test_file_constraints(self) -> None:
        # Test File path constraints
        home_ssh = subconstraints.FilePath("~/.ssh")
        home_txt = subconstraints.FilePath("~/important.txt")
        assert home_ssh != home_txt
        assert home_ssh == home_ssh

        # Test File permission constraints
        wildcard = subconstraints.FilePermission("*", "diana", "diana")
        all_access = subconstraints.FilePermission("777", "diana", "diana")
        all_local_access = subconstraints.FilePermission("700", "diana", "diana")
        assert wildcard.is_compatible(all_access)
        assert not all_access.is_compatible(all_local_access)

        # Test File content constraints
        ssh_key_content = subconstraints.FileContent(subconstraints.FileContent.ADD_SSH_PUBLIC_KEY, True,
                                                     "ADD_SSH_PUBLIC_KEY", "*")
        password_ssh = subconstraints.FileContent(subconstraints.FileContent.SSH_ENABLE_PASSWORD, False)
        assert ssh_key_content.is_compatible(ssh_key_content)
        assert not ssh_key_content.is_compatible(password_ssh)


    def test_secret_preconditions(self) -> None:
        # Testing a procedure that has requirements against a transition with none
        n2 = sg.Node("Bear", "SuperUser")
        no_secret_transition = sg.Transition(n2, n2, "T1552")
        one_secret_transition = sg.Transition(n2, n2, "T1552", requires=["secret"])
        wildcard_secret_required = [{"Type": "SSH_KEY_PAIR", "Amount": "*"}]
        # Testing a procedure that has matching requirements (and no secret generated)
        no_sec_precondition = constraints.SecretPreconditions([], [])
        wildcard_secret_precondition = constraints.SecretPreconditions(wildcard_secret_required, [])
        assert no_sec_precondition.is_eligible(no_secret_transition, {})
        # Testing a procedure that doesn't require any secret but the transition requires one.
        assert not no_sec_precondition.is_eligible(one_secret_transition, {})
        # Testing a procedure that requires * amount of secrets but the transition requires one.
        assert wildcard_secret_precondition.is_eligible(one_secret_transition, {})
        # Testing a procedure that has the wrong secret type (secret has already been generated)
        filled_secret_dic = {"secret": secret_generation.Secret("secret",
                                                                subconstraints.SecretType("PLAINTEXT_PASSWORD"), "a")}
        assert not wildcard_secret_precondition.is_eligible(one_secret_transition, filled_secret_dic)
        # Testing a procedure that has the right secret type (secret has already been generated)
        filled_ssh_secret_dic = {"secret": secret_generation.Secret("secret",
                                                                    subconstraints.SecretType("SSH_KEY_PAIR"), "a")}
        assert wildcard_secret_precondition.is_eligible(one_secret_transition, filled_ssh_secret_dic)
