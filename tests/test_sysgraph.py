"""Tests for the SystemGraph class."""
import chouchen.system_graph as sg
import os
import unittest
import chouchen.graphics


def dummy_scenario() -> sg.SystemGraph:
    """Creates a simple scenario for testing purposes.
    This contains 2 machines, 3 users, 3 transitions, 2 of them related to secrets.
    
    Returns:
        a SystemGraph object corresponding to this scenario.
    """
    n1 = sg.Node("Bear", "Alice")
    n2 = sg.Node("Bear", "SuperUser")
    n3 = sg.Node("Skunk", "Diana")

    t1 = sg.Transition(n1, n2, "T1548")
    t2 = sg.Transition(n2, n2, "T1552", rewards=["skunk_secret"])
    t3 = sg.Transition(n2, n3, "T1021", requires=["skunk_secret"])

    nodes = [n1, n2, n3]
    transitions = [t1, t2, t3]
    starting = [n1]
    victory = [n3]
    sys_graph = sg.SystemGraph(nodes, starting, victory, transitions)
    return sys_graph


def dummy_unbeatable_scenario() -> sg.SystemGraph:
    """Creates a simple scenario that is unbeatable for testing purposes.
    This contains 2 machines, 3 users, 3 transitions, 2 of them related to secrets.
    This scenario is unbeatable because one of the transitions requires a secret not available in the scenario.
    Returns:
        a SystemGraph object corresponding to this scenario.
        """
    n1 = sg.Node("Bear", "Alice")
    n2 = sg.Node("Bear", "SuperUser")
    n3 = sg.Node("Skunk", "Diana")

    t1 = sg.Transition(n1, n2, "T1548")
    t2 = sg.Transition(n2, n2, "T1552", rewards=[])
    t3 = sg.Transition(n2, n3, "T1021", requires=["skunk_secret"])

    nodes = [n1, n2, n3]
    transitions = [t1, t2, t3]
    starting = [n1]
    victory = [n3]
    sys_graph = sg.SystemGraph(nodes, starting, victory, transitions)
    return sys_graph


class TestScenarioGraph(unittest.TestCase):

    def test_export_import_json(self) -> None:
        # Creates a sample scenario, exports it as test.json, imports it and checks its content
        sys_graph = dummy_scenario()
        sg.export_scenario_as_json(sys_graph, "test.json")
        assert os.path.exists("test.json")
        imported_graph = sg.import_scenario_from_json("test.json")
        assert len(imported_graph.nodes) == 3
        assert len(imported_graph.transitions) == 3
        assert len(imported_graph.starting_nodes) == 1
        assert len(imported_graph.winning_nodes) == 1

    def test_invalid_json(self) -> None:
        with self.assertRaises(FileNotFoundError):
            sg.import_scenario_from_json("a_made_up_file_path")

    def test_winning_scenario(self) -> None:
        sys_graph = dummy_scenario()
        result, transitions = sys_graph.is_winnable()
        assert result
        unbeatable_sys_graph = dummy_unbeatable_scenario()
        result, transitions = unbeatable_sys_graph.is_winnable()
        assert not result

    def test_transition_valid(self) -> None:
        n2 = sg.Node("Bear", "SuperUser")
        n3 = sg.Node("Skunk", "Diana")
        t2 = sg.Transition(n2, n2, "T1552", rewards=["skunk_secret"])
        t3 = sg.Transition(n2, n3, "T1021", requires=["skunk_secret"])
        attack_state = sg.AttackState([n2], [t2], ["skunk_secret"])
        result = attack_state.is_transition_valid(t3)
        assert result

    def test_random_attack_path(self) -> None:
        sys_graph = dummy_scenario()
        attack_path = sg.AttackPath.generate_random_attack_path(sys_graph, 1)
        attack_path_2 = sg.AttackPath.generate_random_attack_path(sys_graph, 20)
        assert len(attack_path.transitions) == 1
        assert len(attack_path_2.transitions) == 3

    def test_graph(self) -> None:
        sys_graph = dummy_scenario()
        chouchen.graphics.draw_graph(sys_graph)
        assert os.path.exists(chouchen.graphics.SAVE_PATH)


if __name__ == '__main__':
    unittest.main()
