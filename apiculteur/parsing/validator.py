#! /usr/bin/env python3
# coding: utf-8

"""
[EN]
This script will validate the scheme and types of the JSON scenario
-------------------------------------------------------------------------------------------------------------
Ce script va valider le schéma et le typage du scénario JSON
[FR]

pip install jsonschema
"""

import json
import logging
import os.path

import jsonschema
import jsonschema.exceptions

log = logging.getLogger(__name__)

# Importing the schema we want to validate
with open(f"{os.path.dirname(__file__)}/new_schema.json", "r") as f:
    scenario_schema = json.load(f)
    f.close()


def validate_json_scenario(j: object) -> bool:
    """Validates the entry json scenario based on the schme astored in ./new_schema.json
    Args:
        j: the dictionary to check against the schmema.
    Returns:
        True if the schema is valid, False otherwise.
    """
    try:
        jsonschema.validate(instance=j, schema=scenario_schema)
    except jsonschema.exceptions.ValidationError as err:
        log.debug(err)
        log.error(
            f"""            
Error while looking for required {err.validator_value} inside the scenario.
{err.message} :
{json.dumps(err.instance, indent=4)}"""
        )
        log.warning(
            'Note that some fields can be replaced by others: you can put either "Image" or "OS".'
        )
        log.warning(
            'Please check the "schema.json" in order to better understand the scenario foramt.'
        )
        return False
    return True
