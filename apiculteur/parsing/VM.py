"""
Object representing a Virtual Machine.
"""

import logging

from apiculteur.virtualbox_interface.VNM import virtual_network_manager

log = logging.getLogger(__name__)


class VirtualMachine:
    """A class to represent Virtual Machine objects. These will then be converted into ansible playbooks and tasks.
    Attributes:
        name: the name of the machine.
        image: the name of the ansible image.
        users: a list of users.
        network_names: a list of network names.
        files: a list of file objects.
        packages: a list of packages.
        repos: a list of repos.
        keys: a list of repo keys.
        inline_commands: a list of inline commands.
        inline_file_commands: a list of inline file commands.
        postlaunch_commands: a list of postlaunch commands (think payload detonation).
        used_ports: a list of used ports.
    """

    def __init__(self) -> None:
        self.name: str = ""
        self.name_no_number: str = ""
        self.image: str = ""
        self.users: list[dict] = []
        self.network_names: list[str] = []
        self.files: list[dict] = []
        self.packages: list[dict] = []
        self.repos: list[dict] = []
        self.keys: list[dict] = []
        self.inline_commands: list[str] = []
        self.inline_file_commands: list[dict] = []
        self.postlaunch_commands: list[dict] = []
        self.used_ports = []
        self.hardcoded_ips = []
        self.global_network_pattern = ""
        self.dns_server_ip = ""
        # Self handled
        self.network_names_to_IP: dict = {}

    ##
    #
    # Setters/ Adders
    #
    ##

    def set_image(self, image: str):
        """Sets value of image attribute.
        Args:
            image: the value to set.
        """
        self.image = image

    def get_image(self) -> str:
        """Gets value of image attribute.
        """
        return self.image

    def add_port(self, port_string: str) -> None:
        """Adds port value to attirbute list of used ports.
        """
        self.used_ports.append(port_string)

    def add_file(
            self,
            dst: str,
            full_path_src: str,
            owner: str,
            group: str,
            perm: str,
            modif: str,
            before_packages: bool,
    ):
        """Add a file entry to the VM with the provided arguments.

        Args:
             dst (str): destination on the guest
             full_path_src (str): full path source on the host
             owner (str): owner
             group (str): group
             perm (str): Permissions for the file (Unix format)
             before_packages (bool): Copy before packages or not
             modif: modification to do to the file (edit/append/write)
        """
        self.files.append(
            {
                "Destination": dst,
                "Source": full_path_src,
                "Owner": owner.lower(),
                "Group": group.lower(),
                "Permissions": perm,
                "before_packages": before_packages,
                "Modification": modif
            }
        )

    def add_file_inline_command(self, dst: str, full_path_src: str, owner: str, group: str, perm: str, modif: str,
                                before_packages: bool):
        """Adds a file inline command to the VM object.
        Args:
            dst (str): destination on the guest
            full_path_src (str): full path source on the host
            owner (str): owner
            group (str): group
            perm (str): Permissions for the file (Unix format)
            before_packages (bool): Copy before packages or not
            modif: modification to do to the file (edit/append/write)
        """
        self.inline_file_commands.append({
            "Destination": dst,
            "Source": full_path_src,
            "Owner": owner.lower(),
            "Group": group.lower(),
            "Permissions": perm,
            "before_packages": before_packages,
            "Modification": modif
        })

    def add_package(self, name: str, version: str, service_name: str = ""):
        """Add a package to the VM with the provided arguments.

        Args:
            * name (str): name of the package
            * version (str): version of the package, also use to know the type (.deb/url/...)
            * service_name (str) (default is null): name of the service associated to the package if exists
        """
        self.packages.append(
            {"name": name, "version": version, "service_name": service_name}
        )

    def add_repo(self, repo: str):
        """Add a repository to the VM with the provided arguments.

        Args:
            * repo (str): link of the repository
        """
        self.repos.append({"repo": repo})

    def add_keys(self, key_id: str, key_server: str):
        """Add a repo keys to the VM with the provided arguments.

        Args:
            * key_id (str): key of the repository
            * key_server (str): server where get the key
        """
        self.keys.append({"key_id": key_id, "key_server": key_server})

    def add_inline_command(self, inline: str):
        """Adds inline command to the VM object.
        Args:
            inline: the inline command to add.
        """
        self.inline_commands.append(inline)

    def add_postlaunch_command(self, command: dict):
        """Adds postlaunch command to the VM object.
        Args:
            command: the postlaunch command to add.
        """
        self.postlaunch_commands.append(command)

    ##
    #
    # Getters
    #
    ##
    def get_users(self) -> list:
        """Returns the list of users of the VM.
        """
        return self.users

    def get_files(self) -> "list[dict]":
        """Return files to be provisioned on the virtual machine

        Returns:
            list[object]: list of files object.

        """
        return self.files

    def get_used_ports(self) -> "list[str]":
        """Return ports used.

        Returns:
            list[object]: list of files object.

        """
        return self.used_ports

    def get_inline_file_commands(self) -> list[dict]:
        """Getter for inline_file_commands attribute.
        Returns:
            the inline_file_commands attribute of the VM.
        """
        return self.inline_file_commands

    def get_provision_networks(self) -> "list[str]":
        """Gets the list of all provision networks of the VM.
        Done by asking the VNM module. Uses VirtualBox.

        Returns:
            The list of all provision networks of the VM.
        """
        return [
            virtual_network_manager.get_provision_network(network)
            for network in self.network_names
        ]

    def get_keys(self) -> "list[dict]":
        """Return keys to be provisioned on the virtual machine.

        This will be a list of key_id, key_server objects.
        For instance: [{"key_id": "36A1D7869245C8950F966E92D8576A8BA88D21E9", "key_server": "keyserver.ubuntu.com"}].

        Returns:
            list[object]: list of keys object (includes key_id and key_server)


        """
        return self.keys

    def get_repos(self) -> "list[dict]":
        """Return repositories to be provisioned on the virtual machine
        For instance: [{"repo": "deb http://archive.canonical.com/ubuntu hardy partner"}]
        Returns:
            list[object]: list of repositories object.


        """
        return self.repos

    def get_ip(self, provision_network: str) -> str:
        """Gets the IP of the VM inside the network indicated by the argument.
        This is done by using the VNM module. Uses VirtualBox.
        Args:
            provision_network: the related provision network.
        Returns:
            the ip of the VM.
        """
        if "network" in self.hardcoded_ips:
            return self.hardcoded_ips["network"]
        for network in self.network_names:
            if (
                    virtual_network_manager.get_provision_network(network)
                    == provision_network
            ):
                if network in self.network_names_to_IP.keys():
                    return self.network_names_to_IP[network]
                else:
                    ip_for_prov_network = (
                        virtual_network_manager.get_free_ip_address_for_network(
                            provision_network
                        )
                    )
                    self.network_names_to_IP[network] = ip_for_prov_network
                    return ip_for_prov_network
        log.error(
            f'Error getting IP address for VM "{self.name}" and provision_network "{provision_network}".'
        )
        exit(1)

    def get_packages(self) -> "list[dict]":
        """Return packages to be provisioned on the virtual machine.
        For instance, [{ "name": "foo", "version": "latest"}]

        Returns:
            list[object]: list of packages object.

        """
        return self.packages

    def get_inline_commands(self) -> "list[str]":
        """Getter for inline commands attribute.
            Returns:
                attribute inline_commands.
        """

        return self.inline_commands

    def get_postlaunch_commands(self) -> list[dict]:
        """Getter for postlaunch_commands attribute.
        Returns:
            attribute postlaunch_commands.
        """
        return self.postlaunch_commands

    def get_services_names(self) -> "list[str]":
        """Gets all the service names of the VM object.
        Returns:
            The list of all service names.
        """
        services = []
        for potential_service in self.get_packages():
            if potential_service["service_name"] != "":
                services.append(potential_service["service_name"])
        return services
