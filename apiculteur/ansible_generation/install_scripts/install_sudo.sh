#!/bin/bash
export SUDO_FORCE_REMOVE=yes
apt purge sudo --assume-yes
wget https://www.sudo.ws/dist/sudo-{{sudo_version}}.tar.gz
tar -xvf sudo-{{sudo_version}}.tar.gz
cd sudo-{{sudo_version}}
./configure --with-secure-path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
make
make install
mv /usr/local/bin/sudo /usr/bin
chmod 4111 /usr/bin/sudo
cd ..
rm -r sudo-{{sudo_version}}tar.gz
rm -r sudo-{{sudo_version}}
