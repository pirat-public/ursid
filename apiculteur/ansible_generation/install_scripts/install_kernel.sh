#!/bin/bash
export SUDO_FORCE_REMOVE=yes

# Remove protection for running kernel
mv /usr/bin/linux-check-removal /usr/bin/linux-check-removal.orig
echo -e '#!/bin/sh\necho "Wiping kernel ... what could go wrong ?"\nexit 0' | sudo tee /usr/bin/linux-check-removal
chmod +x /usr/bin/linux-check-removal

# Uninstall all kernels
apt --assume-yes purge linux-image-*

# Install the new kernel
apt install -y linux-image-{{kernel_version}}-generic

# Restore protection for running kernel
mv /usr/bin/linux-check-removal.orig /usr/bin/linux-check-removal