"""Utility functions to link Vagrant and VM objects.

"""

from apiculteur.parsing.VM import VirtualMachine


def get_vm_playbook_name(vm: VirtualMachine) -> str:
    """Gets the playbook name of the related VM object.
    Args:
        vm: a VirtualMachine object.
    Returns:
        The name of the playbook related to the VM.
    """
    return f"main_playbook_{vm.name}.yml"
