"""Functions related to actions to perform post launch, such as detonating payloads.
NOTE: this hasn't been tested since CERBERE deployment and might be broken.

"""

from apiculteur.virtualbox_interface.VNM import INTERNET_VAGRANT_IGNORE

KEY_PATH = ".vagrant/machines/"
OUTPUT_PATH = "./output"


def post_launch_payload_detonation(vms: list) -> None:
    """Handles post launch commands for VMs, such as for instance detonating paylods.
    NOTE: this hasn't been tested in a while and might be broken.
    Args:
        vms: a list of VM objects
    """
    for vm in vms:
        if vm.postlaunch_commands:
            for command in vm.postlaunch_commands:
                machine_name = vm.name
                user_name = command["User"]
                str_command = command["Command"]
                prov_netw_ssh = ""
                for prov_netw in vm.get_provision_networks():
                    if not prov_netw == INTERNET_VAGRANT_IGNORE:
                        prov_netw_ssh = prov_netw
                        break
                machine_ip = vm.get_ip(prov_netw_ssh)
                key_path = f"""{KEY_PATH}{machine_name}/virtualbox/private_key"""
                bash_command = f"""ssh vagrant@{machine_ip} -i {key_path}
sudo -u {user_name} {str_command}"""
                with open(f"""{OUTPUT_PATH}/{machine_name}_{user_name}.sh """, "w+") as outFile:
                    outFile.write(bash_command)
