"""A module responsible for creating ansible tasks and playbooks related to VM objects given as arguments.

These include:
    - Service tasks to start services by specific users.
    - User tasks to create users.
    - File tasks (append to file, append or create, edit, edit or create, copy file, copy directory)
    - Tasks related to package installations (repository keys, update repository, specific tasks for some packages)
    - Inline file tasks to execute commands contained in files.

"""

import crypt
import glob
import logging
import os
import os.path
import random
import re
import secrets
import shutil
import string

import tzlocal

import apiculteur.ansible_generation.interface_vagrant as int_vagrant
import utils.text as text
from apiculteur.parsing.VM import VirtualMachine
from apiculteur.virtualbox_interface.VNM import INTERNET_VAGRANT_IGNORE

log = logging.getLogger(__name__)
INSTALL_SCRIPT_PATH_FROM_OUTPUT = "../../apiculteur/ansible_generation/install_scripts"
INSTALL_SCRIPT_PATH_FROM_MAIN = "./apiculteur/ansible_generation/install_scripts"
INSTALL_SERVICE_PATH_FROM_MAIN = "./apiculteur/ansible_generation/service_scripts"


# TODO: NPM service script should not depend on folder name


def gen_password_hash(clear_password: str) -> str:
    """Generates a password hash based on the password given in entry.
    Args:
        clear_password: the entry password.
    Returns:
        The hashed password.
    """
    randomsalt = "".join(random.sample(string.ascii_letters, 8))
    return crypt.crypt(clear_password, f"$6${randomsalt}$")


def pre_task() -> str:
    """ Adds tasks that have to be added to every machine.
    For now this only changes the timezone of the machines to match the one of the host.

     Returns:
        A string containing the relevant ansible playbook code.
    """
    local_timezone = tzlocal.get_localzone_name()
    output = f"\n- name: Set timezone to host timezone"
    output += f"\n  become: yes"
    output += f"\n  community.general.timezone:"
    output += f"\n    name: {local_timezone}\n\n"
    return output


def service_task(user: dict, service: str, role_path: str) -> str:
    """Adds a create_service task in the playbook.
    This will be called by user_task if the ["Service"] key in the user is not empty.
    This first adds a script to be executed by the service, then the service file to the proper directory,
    then calls ansible builtin service to run it and enable it on reboot.
    Note: SSH is a special case for now, as this service is available by default. If a User constraint specifies SSH,
    it means that constraint is asking for a SSH restart.

    Args:
        user: A dictionary representing all user constraints.
        service: the name of the service.
        role_path: the root directory of the machine role
    Returns:
        A string containing the relevant ansible playbook code.
    """

    # If there is no service to deal with, end the function
    if "Services" not in user:
        return ""

    # Look for a service file and a script file in /service_scripts.
    service_file = glob.glob(INSTALL_SERVICE_PATH_FROM_MAIN + f"/{service}_service.service")
    script_file = glob.glob(INSTALL_SERVICE_PATH_FROM_MAIN + f"/{service}_service.sh")
    # If there is no match, suppose the user is just asking for a service restart (useful for ssh).
    # Log it to make sure the user is aware of this.
    if not service_file and not script_file:
        output = f"\n- name: Restart {service} service"
        output += f"\n  become: true"
        output += f"\n  ansible.builtin.service:"
        output += f"\n    name: {service}"
        output += f"\n    state: restarted"
        output += f"\n    enabled: yes"
        log.info(f"Files related to service {service} wasn't found, creating restart task only.")
        if service == "bind9":
            # special restart case
            output += f"\n- name: Restarting rndc"
            output += f"\n  become: true"
            output += f"\n  ansible.builtin.shell:"
            output += f"\n    rndc reload"
        return output

    else:
        service_file = service_file[0]
        script_file = script_file[0]
        # If one matches the name, copy them to template.
        template_path_from_main = f"{role_path}/templates/"
        shutil.copy(service_file, template_path_from_main)
        shutil.copy(script_file, template_path_from_main)
        # script_file is now the base name
        script_file = os.path.basename(script_file)
        service_file = os.path.basename(service_file)
        # Add variables to /vars (username, usergroup)
        # ip will be done somewhere else
        var_file_path_from_main = f"{role_path}/vars/main.yml"
        with open(var_file_path_from_main, "a+") as out_file:
            out_file.write(f"\n{service}_user_name : {user['Name']}")
            out_file.write(f"\n{service}_user_group : {user['Group']}")
            out_file.write(f"\n{service}_script_path: /usr/bin/{script_file}")
        # generate the 3 tasks
        output = f"\n- name: Adding script to execute {script_file} to /usr/bin"
        output += f"\n  become: yes"
        output += f"\n  ansible.builtin.template:"
        output += f"\n    src: {script_file}"
        output += f"\n    dest: /usr/bin/{os.path.basename(script_file)}"
        output += f"\n    owner: {user['Name']}"
        output += f"\n    group: {user['Group']}"
        output += f"\n    mode: 0755"

        output += f"\n\n- name: Adding service file to /etc/systemd/system to be executed as user {user['Name']}"
        output += f"\n  become: yes"
        output += f"\n  ansible.builtin.template:"
        output += f"\n    src: {service_file}"
        output += f"\n    dest: /etc/systemd/system/{os.path.basename(service_file)}"
        output += f"\n    owner: root"
        output += f"\n    group: root"
        output += f"\n    mode: 0644"

        output += f"\n\n- name: Launch service and enable it at boot"
        output += f"\n  become: yes"
        output += f"\n  ansible.builtin.service:"
        output += f"\n    name: {os.path.splitext(os.path.basename(service_file))[0]}"
        output += f"\n    state: restarted"
        output += f"\n    enabled: yes"

        return output


def user_task(user) -> str:
    """Adds a create_user task in the playbook.
    If the user has SuperUser privileges, adds a playbook command to add him to sudoers.
    Args:
        user: A dictionary representing all user constraints.
    Returns:
        a string corresponding to the relevant ansible playbook commands.
    """

    return_string = f"""- name: Add user {user["Name"]}
  become: yes
  ansible.builtin.user:
    name: "{user["Name"]}"
    password: "{gen_password_hash(user["Credentials"]["Value"])}"
    shell: "/bin/bash"
    create_home: yes

"""
    if user["Privilege"] == "SuperUser":
        return_string += f"""- name: Adding user {user["Name"]} to sudoers
  become: yes
  ansible.builtin.lineinfile:
    dest: "/etc/sudoers"
    line: "{user["Name"]} ALL=(ALL) NOPASSWD: ALL"
    insertafter: "EOF"

"""
    return_string += change_user_permissions(user["Name"])

    return return_string


def append_file_task(file: dict) -> str:
    """Adds a task appending the content of the file in argument.
    Args:
        file: the file whose content to append.
    Returns:
        The corresponding ansible playbook task.
    """
    with open(file["Source"]) as inFile:
        line_to_append = inFile.read()
    return f"""- name: Append {file["Source"].split("/")[-1]} file to {file["Destination"]} 
  become: yes
  ansible.builtin.lineinfile:
    dest: "{file["Destination"]}"
    line: "{line_to_append}"
    insertafter: "EOF"

"""


def appendplus_file_task(file: dict) -> str:
    """Adds a task appending the content of the file in argument, and creating it if it doesn't exist.
    Args:
        file: the file whose content to append or create.
    Returns:
        The corresponding ansible playbook task.
    """
    with open(file["Source"]) as inFile:
        line_to_append = inFile.read()
    return f"""- name: Make sure {os.path.dirname(file["Destination"])} directory exists
  become: yes
  file:
    path: "{os.path.dirname(file["Destination"])}"
    owner: "{file["Owner"]}"
    group: "{file["Group"]}"
    state: directory
    

- name: Append {file["Source"].split("/")[-1]} file to {file["Destination"]} 
  become: yes
  ansible.builtin.lineinfile:
    dest: "{file["Destination"]}"
    line: "{line_to_append}"
    insertafter: "EOF"
    create: yes

"""


def edit_file_task(file: dict) -> str:
    """Adds a task editing the contents of the file in argument, by looking for a line containing a specific string
    and changing its cnotents.
    Args:
        file: the file whose content to edit.
    Returns:
        The corresponding ansible playbook task.
    """
    with open(file["Source"]) as inFile:
        line_to_edit = inFile.read()
    line_to_find = file["Modification"].split()[1:]
    line_to_find = ' '.join(line_to_find)
    return f"""- name: Edit line containing {line_to_find} of {file["Destination"]} with {file["Source"].split("/")[-1]}
  become: yes
  ansible.builtin.lineinfile:
    dest: "{file["Destination"]}"
    search_string: "{line_to_find}"
    line: "{line_to_edit}"

"""


def editplus_file_task(file: dict) -> str:
    """Adds a task editing the contents of the file in argument, by looking for a line containing a specific string
       and changing its contents. Creates the file if it does not exist.
       Args:
           file: the file whose content to edit or create.
       Returns:
           The corresponding ansible playbook task.
       """
    with open(file["Source"]) as inFile:
        line_to_edit = inFile.read()
    line_to_find = file["Modification"].split()[1]

    return f"""- name: Make sure {os.path.dirname(file["Destination"])} directory exists
  become: yes
  file:
    path: "{os.path.dirname(file["Destination"])}"
    owner: "{file["Owner"]}"
    group: "{file["Group"]}"
    state: directory
    
    
- name: Edit line containing {line_to_find} of {file["Destination"]} with {file["Source"].split("/")[-1]}
  become: yes
  ansible.builtin.lineinfile:
    dest: "{file["Destination"]}"
    create: yes
    search_string: "{line_to_find}"
    line: "{line_to_edit}"
    insertafter: "EOF"

"""


def copy_file_task(file: dict) -> str:
    """Adds a copy_file task in the playbook.

    Args:
        file (object): Object expected to the following format :

        {
            "Destination": "/root/file1",
            "Source": "/full/path/to/file1",
            "Owner": "root",
            "Group": "root",
            "Permissions": "0640",
            "before_packages": True
        }
"""
    return f"""- name: Make sure {os.path.dirname(file["Destination"])} directory exists
  become: yes
  file:
    path: "{os.path.dirname(file["Destination"])}"
    owner: "{file["Owner"]}"
    group: "{file["Group"]}"
    state: directory

- name: Copy {file["Source"].split("/")[-1]} file to {file["Destination"]} 
  become: yes
  ansible.builtin.copy:
    src: "{os.path.basename(file["Source"])}"
    dest: "{file["Destination"]}"
    owner: {file["Owner"]}
    group: {file["Group"]}
    mode: {file["Permissions"]}

"""


def append_beginning_template_task(file: dict, role_path: str) -> str:
    """Appends content of template file to destination.
    Args:
        file: File object.
        role_path: path of current role.

    """

    template_path_from_main = f"""{role_path}/templates/"""
    shutil.move(file['Source'], template_path_from_main + os.path.basename(file["Source"]))
    file["Source"] = template_path_from_main + os.path.basename(file["Source"])

    return f"""- name: Append template file {os.path.basename(file['Source'])} to beginning
  become: yes   
  blockinfile:
    insertbefore: BOF
    path: "{file["Destination"]}"
    block: "{{{{ lookup('template', '{os.path.basename(file['Source'])}') }}}}"
    marker: ""
"""


def write_template_task(file: dict, role_path: str) -> str:
    """Adds a copy_file task in the playbook. The file will be put as a template.

    Args:
        file (object): Object expected to the following format :
        role_path: path of the current role
        {
            "Destination": "/root/file1",
            "Source": "/full/path/to/file1",
            "Owner": "root",
            "Group": "root",
            "Permissions": "0640",
            "before_packages": True
        }
"""

    # Move file to templates file
    template_path_from_main = f"""{role_path}/templates/"""
    shutil.move(file['Source'], template_path_from_main + os.path.basename(file["Source"]))
    file["Source"] = template_path_from_main + os.path.basename(file["Source"])

    output_string = f"""- name: Make sure {os.path.dirname(file["Destination"])} directory exists
  become: yes
  file:
    path: "{os.path.dirname(file["Destination"])}"
    owner: "{file["Owner"]}"
    group: "{file["Group"]}"
    state: directory
"""

    output_string += f"\n- name: Copy template file {os.path.basename(file['Source'])}"
    output_string += f"\n  become: yes"
    output_string += f"\n  ansible.builtin.template:"
    output_string += f"\n    src: {os.path.basename(file['Source'])}"
    output_string += f"\n    dest: {file['Destination']}"  # will install in tmp directory
    output_string += f"\n    owner: {file['Owner']}"  # if you want to change that you have to edit scripts
    output_string += f"\n    group: {file['Group']}"
    output_string += f"\n    mode: {file['Permissions']}\n"
    return output_string


def copy_directory_task(file: dict) -> str:
    """Adds a copy_directory task in the playbook
    We can't use ansible.copy because it's so slow on directories, so we use synchronize.
    But synchronize can't set permissions so we have to make that a separate task (fun).

    Args:
        file (object): Object expected to the following format :

        {
            "Destination": "/root/file1",
            "Source": "/full/path/to/file1",
            "Owner": "root",
            "Group": "root",
            "Permissions": "0640",
            "before_packages": True
        }
"""

    # First check the last character of the destination
    # If it's NOT a "/", remove the last part of the path
    # For instance, with src: foo/bar and dest: toto
    # the expected result is actually for the directory to be at foo/toto
    # However, with src: foo/bar/ and dest: toto
    # the expected result is foo/bar/toto

    if file["Destination"][-1] != "/":
        file["Destination"] = os.path.split(file["Destination"])[0]

    return f"""- name: Make sure {os.path.dirname(file["Destination"])} directory exists
  become: yes
  file:
    path: "{os.path.dirname(file["Destination"])}"
    owner: "{file["Owner"]}"
    group: "{file["Group"]}"
    state: directory

- name: Copy {file["Source"].split("/")[-1]} directory to {file["Destination"]} 
  become: yes
  ansible.posix.synchronize:
    dest: "{file["Destination"]}"
    src: "{os.path.basename(file["Source"])}"
    use_ssh_args: true

- name: Change {file["Source"].split("/")[-1]} permissions
  become: yes
  ansible.builtin.file:
    recurse: yes
    path: "{file["Destination"]}"
    owner: "{file["Owner"]}"
    group: "{file["Group"]}"
    mode: "{file["Permissions"]}"

"""


def add_repo_task(repo: dict) -> str:
    """Adds a add_repo task in the playbook

    Args:
        repo(object): Object expected to the following format :

        {
            "repo": "deb http://archive.canonical.com/ubuntu hardy partner"
        }
    """
    return f"""- name: Add specified repository into sources list
  become: yes
  ansible.builtin.apt_repository:
    repo: {repo["repo"]}
    state: present

"""


def add_key_task(key: dict) -> str:
    """Adds a add_key task in the playbook

    Args:
        key (object): Object expected to the following format :

        {
            "key_id": "36A1D7869245C8950F966E92D8576A8BA88D21E9"
            "key_server": "keyserver.ubuntu.com"
        }
    """
    return f"""- name: Add an apt key by id from {key["key_server"]}
  become: yes
  ansible.builtin.apt_key:
    keyserver: {key["key_server"]}
    id: {key["key_id"]}

"""


def aux_install_package(package: dict, role_path: str):
    """ Specific installation instructions for some packages.
    Args:
        package: a package.
        role_path: the root directory of the machine role
    """
    script_path_from_main = f"""{INSTALL_SCRIPT_PATH_FROM_MAIN}/install_{package["name"]}.sh"""
    template_path_from_main = f"""{role_path}/templates/"""
    var_path_from_main = f"""{role_path}/vars/main.yml"""
    shutil.copy(script_path_from_main, template_path_from_main + f"install_{package['name']}.sh")
    # Add variable to vars directory
    if package["version"] == "*":
        package["version"] = "'*'"  # might break in the future
    with open(var_path_from_main, "a+") as out_file:
        out_file.write(f"\n{package['name']}_version: {package['version']}")

    output_string = f"\n- name: Copy install file {package['name']} at version {package['version']}"
    output_string += f"\n  become: yes"
    output_string += f"\n  ansible.builtin.template:"
    output_string += f"\n    src: install_{package['name']}.sh"
    output_string += f"\n    dest: /tmp/install_{package['name']}.sh"  # will install in tmp directory
    output_string += f"\n    owner: root"  # if you want to change that you have to edit scripts
    output_string += f"\n    group: root"
    output_string += f"\n    mode: 0755"

    output_string += f"\n- name: Install {package['name']} at version {package['version']}"
    output_string += f"\n  become: true"
    output_string += f"\n  shell:"
    output_string += f"\n    /tmp/install_{package['name']}.sh\n"

    if package['name'] == "kernel":
        output_string += f"\n- name: Reboot to apply new kernel"
        output_string += f"\n  become: true"
        output_string += f"\n  ansible.builtin.reboot:\n"
    return output_string


def install_package_task(package: dict, role_path: str) -> str:
    """Adds a install_package task in the playbook

    Args:
        role_path: the root directory of the role
        package (object): Object expected to the following format :

        {
            "name": "foo"
            "version": "latest"
        }
    """
    # TODO: more elegant way of handling file path
    SUPPORTED_EDGE_CASES = ["sudo", "npm", "python", "kernel", "systemd-resolved", "netcat"]
    if package["name"] in SUPPORTED_EDGE_CASES:
        return aux_install_package(package, role_path)

    if package["name"] == "postfix":
        return f"""- name: Set hostnamectl
  become: yes
  shell: hostnamectl set-hostname bastion.casinolimit.bzh

- name: Edit /etc/hosts
  become: yes
  shell: | 
    echo '127.0.0.1 bastion.casinolimit.bzh casinolimit.bzh' >> /etc/hosts
    sudo systemctl restart systemd-networkd

- name: Set Postfix option hostname
  become: yes
  debconf:
    name: postifx
    question: postfix/mailname
    value: casinolimit.bzh
    vtype: string

- name: Set Postfix option type as internet site
  become: yes
  debconf:
    name: postfix
    question: postfix/main_mailer_type
    value: Internet Site
    vtype: select
                  
- name: Set Postfix option root and postmaster mail recipient
  become: yes
  debconf:
    name: postfix
    question: postfix/root_address
    value: admin
    vtype: string

- name: Set Postfix option mail recipient
  become: yes
  debconf:
    name: postfix
    question: postfix/destinations
    value: $myhostname, bastion.casinolimit.bzh, casinolimit, casinolimit.bzh, localhost.casinolimit.bzh, localhost
    vtype: string                 
        
        
- name: Install package {package["name"]}
  become: yes
  apt: 
    name: {package["name"]}
    allow_unauthenticated : yes
    update_cache: yes
    update_cache_retries: 10
    update_cache_retry_max_delay: 300

- name: patch postfix conf to remove bounced emails (bounce)
  become: yes
  replace:
    path: /etc/postfix/master.cf
    regexp:  '^(bounce.*)(bounce)$'
    replace: '\\1discard'

- name: patch postfix conf to remove bounced emails (defer)
  become: yes
  replace:
    path: /etc/postfix/master.cf
    regexp:  '^(defer.*)(bounce)$'
    replace: '\\1discard'

- name: reload postfix
  become: yes
  systemd:
    name: postfix
    state: reloaded

"""

    if package["version"] == "latest" or package["version"] == "*":
        return f"""- name: Install package {package["name"]}
  become: yes
  apt: 
    name: {package["name"]}
    allow_unauthenticated : yes
    update_cache: yes
    update_cache_retries: 10
    update_cache_retry_max_delay: 300

"""

    if package["version"] == "deb":
        return f"""- name: Install debian package {package["name"].split("/")[-1]}
  become: yes
  apt:
    deb: {package["name"]}
    allow_unauthenticated : yes
    update_cache_retries: 10
    update_cache_retry_max_delay: 300

"""
    else:
        return f"""- name: Install version {package["version"]} of package {package["name"]}
  become: yes
  apt: 
    name: {package["name"]}={package["version"]}
    update_cache: yes
    allow_unauthenticated : yes
    update_cache_retries: 10
    update_cache_retry_max_delay: 300

"""


def install_package_post_task(package: dict) -> str:
    """Adds post tasks needed to install a package properly (usually inline commands)

    Args:
        package (object): Object expected to the following format :

        {
            "name": "foo"
            "version": "latest"
        }
    """
    SUPPORTED_EDGE_CASES = ["auditd", "tcpdump", "postfix"]
    if package["name"] not in SUPPORTED_EDGE_CASES:
        return ""
    else:
        match package["name"]:
            case "auditd":
                # idk why this doesn't work automatically
                # redirect to /dev/null to not trigger ansible errors if sth in the rules is not relevant
                force_load = f"\n\n- name: auditd force load"
                force_load += f"\n  become: yes"
                force_load += f"\n  ansible.builtin.command: 'auditctl -R /etc/audit/rules.d/audit.rules'"
                force_load += f"\n  ignore_errors: true"
                return force_load

            case "postfix":
                # idk why this doesn't work automatically
                # redirect to /dev/null to not trigger ansible errors if sth in the rules is not relevant
                postfix_config = f""" 
- name: Postfix various commands
  become: true
  shell: |
    sudo postconf -e 'home_mailbox= Mail/'  
    sudo postconf -e 'virtual_alias_maps= hash:/etc/postfix/virtual' 
    sudo postmap /etc/postfix/virtual
    sudo systemctl restart postfix
    sudo ufw allow Postfix 
    echo 'export MAIL=~/Mail' | sudo tee -a /etc/bash.bashrc | sudo tee -a /etc/profile.d/mail.sh
    . /etc/profile.d/mail.sh 

- name: Execute script to populate with mails
  become: true
  ansible.builtin.shell:
    find /tmp/. -name "*mail*.sh" | /bin/sh
    
"""
                return postfix_config

            case "tcpdump":
                # Launch tcpdump as background task and write output to /var/log/attackPackets
                # TODO: THIS DOES NOT WORK somehow?? idk why
                tcp_dump_launch = f"""- name: Launch tcpdump as background task
  become: true
  ansible.builtin.command: 'tcpdump -i any -w /var/log/attackPackets.pcap &'
  async: 3600
  poll: 0
  ignore_errors: true

"""
                return tcp_dump_launch


def inline_files_task(file: dict, role_path: str):
    """Takes a file object and uses its information to execute a script from the correct location with the correct
    privileges on the resulting machine. These files are assumed to be templates.

      Args:
         file (dict): File object with Destination, Source, Owner, Group, Permissions, before_packages fields.
         role_path: path of the role (useful for templates)
    """

    template_path_from_main = f"""{role_path}/templates/"""
    shutil.move(file['Source'], template_path_from_main + os.path.basename(file["Source"]))
    file["Source"] = template_path_from_main + os.path.basename(file["Source"])

    return f"""- name: Copy script {os.path.basename(file["Source"])} at destination {file["Destination"]} with execution privileges.
  become: true
  ansible.builtin.template:
    src: {os.path.basename(file["Source"])}
    dest: {file["Destination"]}
    mode: '0777'
 
- name: Execute script {file["Destination"]} as user {file["Owner"]}
  become: true
  become_user: {file["Owner"]}
  ansible.builtin.command:
    cmd: ./{os.path.basename(file["Destination"])}
    chdir: {os.path.dirname(file["Destination"])}

- name: delete installation script {file["Destination"]}
  become: true
  ansible.builtin.file:
    path: {file["Destination"]}
    state: absent
    
"""


def inline_task(inline: str, user: str = None) -> str:
    """Ansible task to run inline command

    Args:
        inline (str): command
        user: the user related to the task

    Returns:
        str: ansible task
    """
    # The magic number MAX_INLINE_LENGTH is used to shorten the task's written name
    MAX_INLINE_LENGTH = 20
    if user:
        return f"""- name: Execute inline command "{inline if len(inline) <= MAX_INLINE_LENGTH else f"{inline[:MAX_INLINE_LENGTH]}..."}"
          become: yes
          become_user: {user}
          ansible.builtin.shell:
            cmd: "{inline}"

        """
    else:
        return f"""- name: Execute inline command "{inline if len(inline) <= MAX_INLINE_LENGTH else f"{inline[:MAX_INLINE_LENGTH]}..."}"
  become: yes
  ansible.builtin.shell:
    cmd: "{inline}"

"""


def file_task(file: dict, role_path: str) -> str:
    """Creates an ansible task related to the input file modification type.
    Raises a ValueError if the type of modification for the file is not recognized.
    Args:
        file: The file to write a task for.
        role_path: path of the role (useful for templates)
    Returns:
        The ansible playbook task.
    """
    # checks the type of file task
    task_type = file["Modification"].split()[0]
    # checks whether this is a file or directory
    is_directory = os.path.isdir(file["Source"])

    if is_directory:
        return copy_directory_task(file)

    match task_type:
        case "APPEND":
            return append_file_task(file)
        case "APPEND+":
            return appendplus_file_task(file)
        case "APPEND_BEGINNING_TEMPLATE":
            return append_beginning_template_task(file, role_path)
        case "WRITE":
            return copy_file_task(file)
        case "EDIT+":
            return editplus_file_task(file)
        case "EDIT":
            return edit_file_task(file)
        case "WRITE_TEMPLATE":
            return write_template_task(file, role_path)
        case "DELETE":
            return delete_task(file)
        case _:
            raise ValueError("Error: Task type not recognized.")


def restart_service_task(service_name: str) -> str:
    """Adds a task to restart a service.

    Args:
        service_name: the name of the service to restart.
    Returns:
        The corresponding ansible task.
    """
    return f"""- name: Restart "{service_name}" service
  become: yes
  ansible.builtin.service:
    name: {service_name}
    state: restarted
    enabled: yes

"""


def port_task(port_name: str) -> str:
    """Adds a task to opena  port on firewall.

    Args:
        port_name: the name of the port to open.
    Returns:
        The corresponding ansible task.
    """
    return f"""\n- name: Allow all access to tcp port {port_name}
  become: true
  ufw:
    rule: allow
    port: {port_name}
    proto: tcp

"""


def uninstall_ubuntu_advantage() -> str:
    """Adds task to uninstall ubuntu advantage package.
    This is done in order to avoid getting 401 errors on old Ubuntu versions while installing packages,
    as it will otherwise try to install package versiosn only available for ubuntu premium paid users.
    Returns:
        A string corresponding to an ansible playbook tasK.
    """
    return f"""- name: Uninstall ubuntu advantage
  become: yes
  ansible.builtin.apt:
    name: ubuntu-advantage-tools
    state: absent
    update_cache_retries: 10
    update_cache_retry_max_delay: 300

"""


def change_user_permissions(username: str) -> str:
    """Adds task to change a user permissions to 750 so they do not distract from the rest.
    Useful to for default users (ubuntu, debian) that come prepackaged with vagrant boxes.
    """
    return f"""- name: Check whether {username} directory exists.
  become: yes
  ansible.builtin.stat:
    path: /home/{username}
  register: directory_exists
  
- name: Change {username} permissions to 750 if exists.
  become: yes
  ansible.builtin.file:
    path: /home/{username}
    mode: 0750
  when: directory_exists.stat.exists

"""


def delete_task(file: dict) -> str:
    """Adds a delete_directory (or file) task in the playbook

    Args:
        file (object): Object expected to the following format :

        {
            "Destination": "/root/file1",
            "Source": "/full/path/to/file1",
            "Owner": "root",
            "Group": "root",
            "Permissions": "0640",
            "before_packages": True
        }
"""
    return f"""- name: Delete {os.path.dirname(file["Destination"])} directory or file if it exists
  become: yes
  file:
    path: "{os.path.dirname(file["Destination"])}"
    state: absent

"""


def generate_vm_role(vm: VirtualMachine, dest_dir: str, vms, ignore_virtualbox) -> None:
    """Generates role containing tasks for the VM vm.
    The output will be written in directory dest_dir.
    Args:
        vm: the VirtualMachine object to write tasks for.
        dest_dir: path of the output directory.
        vms: list of all VirtualMachines (useful for vars)
        ignore_virtualbox: ignores virtualbox commands if true (take care of your networks yourself, might break)
    """
    log.debug("Generating role for " + vm.name)

    # generate main playbook
    playbook_dest_path = dest_dir + f"/{int_vagrant.get_vm_playbook_name(vm)}"
    with open(playbook_dest_path, "w+") as out_file:
        out_file.write("- name: Main playbook\n")
        out_file.write(f"  hosts: {vm.name}\n")
        out_file.write("  roles:\n")
        out_file.write(f"   - {vm.name}\n")

    # generate directory for the machine role
    os.makedirs(dest_dir + f"/roles/{vm.name}")
    role_path = dest_dir + f"/roles/{vm.name}"
    # generate files folder and copy needed files there
    # PATH BEFORE GENERATION: /output/scenario_i contains playbooks and Vagrantfile
    # /output/scenario_i/generated_files/machine_name (no number) contains files
    # GOAL: /output/scenario_i/roles/machine_name (with number) contains most things
    # /output/scenario_i contains Vagrant + playbooks

    # create folder
    # note: generated files are moved at the end
    os.makedirs(dest_dir + f"/roles/{vm.name}/files")

    # create a task directory and redirect all future writes there
    os.makedirs(dest_dir + f"/roles/{vm.name}/tasks")
    task_dest_path = dest_dir + f"/roles/{vm.name}/tasks/main.yml"

    # create templates directory
    os.makedirs(dest_dir + f"/roles/{vm.name}/templates")

    # create vars directory
    os.makedirs(dest_dir + f"/roles/{vm.name}/vars")

    # add ips as var
    ip_to_write = ""
    for virtual_machine in vms:
        if virtual_machine.hardcoded_ips:
            ip_to_write += f'IP_{virtual_machine.name_no_number}: {virtual_machine.hardcoded_ips[0]}\n'
        elif not ignore_virtualbox:
            for prov_netw in virtual_machine.get_provision_networks():
                if not prov_netw == INTERNET_VAGRANT_IGNORE:
                    ip_to_write += f'IP_{virtual_machine.name_no_number}: {virtual_machine.get_ip(prov_netw)}\n'
                    break
    if not ignore_virtualbox:
        for prov_netw in vm.get_provision_networks():
            if not prov_netw == INTERNET_VAGRANT_IGNORE:
                ip_to_write += f'machine_ip: {vm.get_ip(prov_netw)}\n'
                break  # if there's more than one network we assume any of them will do?
                # might break in the future
    with open(dest_dir + f"/roles/{vm.name}/vars/main.yml", "a+") as out_file:
        out_file.write(ip_to_write)
        if vm.dns_server_ip:
            out_file.write(f"dns_server_ip: {vm.dns_server_ip}")

    main_playbook = open(task_dest_path, "w")

    main_playbook.write(pre_task())
    if vm.global_network_pattern:
        global_network_pattern = vm.global_network_pattern
    else:
        # default case for local tests
        global_network_pattern = "192.168.*.*"
    # Write machine IP in machine_ip variable
    # This will take the first IP that matches the pattern

    for name in vm.network_names:
        i = int(re.search(r'\d+', name).group())
        main_playbook.write(f"""- name: Run a shell command to get ip for network_{i} and register its output as a variable
  become: yes
  shell: echo "$(/sbin/ip -o -4 addr list | awk '{{print $4}}' | cut -d/ -f1 | grep {global_network_pattern} | sed -n {i}p)"
  register: temp_ip_network{i}
- set_fact:
    machine_ip_network{i}: "{{{{ temp_ip_network{i}.stdout }}}}"

""")

    if vm.dns_server_ip:
        main_playbook.write(f"""- name: Retrieve DNS IP
  become: yes
  shell: echo "$(/sbin/ip -o addr list | grep -m 1 {global_network_pattern} | awk '{{print $4}}' | cut -d/ -f1 | cut -d. -f3)"
  register: subnet_id
- set_fact:
    subnet_id: "{{{{ subnet_id.stdout }}}}"
    dns_ip: "{{{{ dns_server_ip | replace('*', subnet_id.stdout) }}}}"
""")

    # Do OS specific actions
    if vm.get_image().find("ubuntu") != -1:
        # need to disable the ubuntu paid service
        # otherwise packages try to install from their private repository
        # it's very dumb
        main_playbook.write(uninstall_ubuntu_advantage())
        # also change permissions for default ubuntu user if it exists
        main_playbook.write(change_user_permissions("ubuntu"))

    if vm.get_image().find("debian") != -1:
        # change permissions of default debian user
        main_playbook.write(change_user_permissions("debian"))

    # Add users
    for user in vm.get_users():
        main_playbook.write(user_task(user))

    # Add gpg package if keys need to be add (necessary to add repository keys)
    if len(vm.get_keys()) > 0:
        main_playbook.write(
            install_package_task({"name": "gpg", "version": "latest"}), role_path)

    # Add repositories keys
    for key in vm.get_keys():
        main_playbook.write(add_key_task(key))

    # Add repositories
    for repo in vm.get_repos():
        main_playbook.write(add_repo_task(repo))

    # Copy files before packages installation
    # Example : .deb
    for file in vm.get_files():
        if file["before_packages"]:
            main_playbook.write(file_task(file, role_path))

    # Packages installation
    for package in vm.get_packages():
        main_playbook.write(install_package_task(package, role_path))

    # Opening ports for firewall
    for port in vm.get_used_ports():
        main_playbook.write(port_task(port))

    # Copy files after packages installation
    # Example : conf file mariadb.conf
    for file in vm.get_files():
        if not file["before_packages"]:
            main_playbook.write(file_task(file, role_path))

    # Add inline commands
    for inline in vm.get_inline_commands():
        main_playbook.write(inline_task(inline))

    for file in vm.get_inline_file_commands():
        main_playbook.write(inline_files_task(file, role_path))

    # Add services attached to users
    for user in vm.get_users():
        if "Services" in user:
            for service in user["Services"]:
                main_playbook.write(service_task(user, service, role_path))

    # Write task to call post-tasks
    main_playbook.write("""\n\n- name: Launch post tasks
  import_tasks: post_tasks.yml
 """)

    # create handler folder for post tasks
    post_tasks_path = dest_dir + f"/roles/{vm.name}/tasks/post_tasks.yml"
    post_tasks_playbook = open(post_tasks_path, "w+")

    # Switch to post_tasks
    post_tasks_playbook.write(f"---")

    # Add services restart
    # not sure what this does, student implem not really used anymore
    for service in vm.get_services_names():
        post_tasks_playbook.write(text.indent_str(restart_service_task(service)))

        # Run any post_task install commands linked to packages
    for package in vm.get_packages():
        post_tasks_playbook.write(install_package_post_task(package))

    # change vagrant user password
    new_password = secrets.token_urlsafe(16)
    hashed_password = gen_password_hash(new_password)
    post_tasks_playbook.write(f"""\n\n- name: Get list of users
  become: yes
  ansible.builtin.getent:
    database: passwd""")
    post_tasks_playbook.write(f"""\n\n- name: Change vagrant password
  become: yes
  ignore_errors: true
  ansible.builtin.user:
    name: vagrant
    password: {hashed_password}
  when:
    - "'vagrant' in ansible_facts.getent_passwd"
""")

    # clear /tmp directory
    post_tasks_playbook.write(f"""\n\n- name: Clear /tmp directory
  become: yes
  ansible.builtin.shell: /bin/rm -rf /tmp/*""")

    # move every generated file to the proper directory now that we don't need them
    # there's prob a better way to do this
    # if files were generated
    if os.path.isdir(dest_dir + "/generated_files/" + re.sub(r'\d+$', '', vm.name)):
        for file_or_dic in os.listdir(dest_dir + "/generated_files/" + re.sub(r'\d+$', '', vm.name)):
            file_full_path = dest_dir + "/generated_files/" + re.sub(r'\d+$', '',
                                                                     vm.name) + "/" + file_or_dic
            shutil.move(file_full_path, dest_dir + f"/roles/{vm.name}/files")


def generate_all_vm_roles(vms: "list[VirtualMachine]", dest_dir: str, ignore_virtualbox: bool):
    """
    Generate a main_playbook.yml from a list of virtual machine objects

    Args:
        `vms` (list[VirtualMachine]): list of virtual machine to declare
        `dest_dir` (str): Destination root folder.
        ignore_virtualbox: will not make us of virtualbox commands (take care of your networks yourself)
    """

    if not os.path.exists(dest_dir):
        log.info(f"Creating folder(s) {dest_dir}")
        os.makedirs(dest_dir)

    # Create role directory
    os.makedirs(dest_dir + "/roles/")

    for vm in vms:
        generate_vm_role(vm, dest_dir, vms, ignore_virtualbox)  # vms is useful to get the ip of everybody involved
