#!/bin/bash
cd /home/{{django_user_name}}/django_website_with_directory_traversal_rewarding_ssh_key
pip3 install pipenv
pip3 install pipenv # do it twice coz pip is a jerk and uses /tmp??
/home/{{django_user_name}}/.local/bin/pipenv install
/home/{{django_user_name}}/.local/bin/pipenv run python manage.py migrate
/home/{{django_user_name}}/.local/bin/pipenv run python3 manage.py loaddata ssh_key.json
/home/{{django_user_name}}/.local/bin/pipenv run python3 manage.py createsuperuser --noinput
ip4={{machine_ip}}
/home/{{django_user_name}}/.local/bin/pipenv run python manage.py runserver "$ip4":8000 &