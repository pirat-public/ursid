#!/bin/bash
var=$(find /home/{{ npm_user_name }}/ -type f -name "app.js")
var=$(dirname "$var")
cd $var
npm install
ip4={{ machine_ip }}
sed -i -e "s/tochange/$ip4/g" ./.env
npm start