#!/bin/bash
sudo systemctl restart docker
var=$(find /home/{{ docker_user_name }}/ -type f -name "compose.yml")
var=$(dirname "$var")
cd $var
docker-compose up