### MISCALLENEOUS notes about the implementation
- The package rsync has to be installed on machines in order to use the ansible.posix.synchronzie module to copy files.
- This is done because the default way of copying files (ansible.copy) is absurdly slow on directories, taking up to 10 min to copy a 6 Mo directory with 1k files in it.
- This also means permissions have to be manually modified after copying the files, as the synchronize module cannot manually set permissions itself (it just copies the existing ones).


### Launching services as users
Services have to be specified in the json scenario for specific user. For instance, in order to run an npm service as user *alice*, use
add
```json
"Users": [
      {
        "Name": "alice",
        ...
        "Services": "npm"
]
``` 
Services are installed on machines by creating 2 files:
- A .service file, containg information about the service itself. For instance the .service file related to the npm service will be named *npm_service.service*.
    - This template for this file must be written in src/ansible_generation/service_scripts. 
    - Some of the values in the service (such as the user and group) launching it, or the path of the associated script) will have to be determined during the generation.
    - Use {username}, {usergroup} and /usr/bin/{script_path} for these values (unless you know what you're doing).
    - Apiculteur will replace the values during the generation and generate the appropriate file in src/ansible_generation/service_scripts/generated.
- A .sh file, containing the script that will be launched by the service on startup.
  - Similarly, {username} or {usergroup} may be used if needed.
  - If the script contains {} characters, you have to escape them by adding additional curly braces around them {{}}.

To create a new service:
    - Create the .sh and .service file templates in src/ansible_generation/service_scripts, using the above instruction and existing examples as a guideline.
    - Add the service to the user in your json scenario.
