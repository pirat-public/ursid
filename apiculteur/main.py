"""Main module of Apiculteur.
Used to convert a procedurally refined scenario (written as a json) into the appropriate Vagrant and Ansible files.

"""
import argparse
import copy
import glob
import logging
import os.path
import shutil
# !/usr/bin/env python3
import subprocess
from os import listdir
from os.path import isfile, join

import apiculteur.ansible_generation.ansible_generator as ag
import apiculteur.parsing.parser as parser
import apiculteur.vagrant_generation.vagrant_generator as vg
import utils.log
import utils.user_confirm
from utils.colors import *
from utils.static import static

project_name = "Apiculteur"
log = logging.getLogger(__name__)


def clean_target_dir(dest_dir: str):
    """Will clear a former Vagrant project by :
    * Running `vagrant destroy`
    * Deleting the old project files

    Args:
        dest_dir (str): target output directory.
    """
    log.warning(f'Running `vagrant destroy` inside "{dest_dir}" folder')
    cleaning = subprocess.Popen(
        "vagrant destroy --no-tty --force", cwd=dest_dir, shell=True
    )
    cleaning.wait()
    log.warning(
        f'Running `rm -Rf .vagrant Vagrantfile main_playbook_*` inside "{dest_dir}" folder'
    )
    cleaning = subprocess.Popen(
        "rm -Rf .vagrant Vagrantfile main_playbook_*", cwd=dest_dir, shell=True
    )
    cleaning.wait()


def verify_target_dir(dest_dir: str):
    """Will check if another vagrant project is running inside the
    target directory.
    If so it will propose to the user to run `vagrant destroy` inside
    this project before generating a new Vagrantfile.

    Args:
        dest_dir (str): target output directory.
    """
    if not os.path.exists(dest_dir):
        return
    if not os.path.exists(f"{dest_dir}/Vagrantfile"):
        return
    if not os.path.exists(f"{dest_dir}/.vagrant"):
        return
    log.warning(
        f"""There already is a Vagrant project that has been run before inside {dest_dir} folder.
It is recommended to perform a `vagrant destroy` command inside this project before anything."""
    )
    if utils.user_confirm.user_confirmation(
            f"Would you like {project_name} to clear the former project before generating a new one ?"
    ):
        clean_target_dir(dest_dir)
    else:
        exit(1)


def build_vagrant_ansible(args: argparse.Namespace) -> None:
    """Builds Vagrant and Ansible files related to the scenario provided in the arguments.

    This module does, in order:
    - Gets the list of all scenarios in the given output directory by parsing and
    looking for output_scenario.json files.
    - Clears the previously generated service and install script folders.
    - Generates Vagrantfiles.
    - Generates playbooks.
    - Generates postlaunch scripts (such as payload detonation, not used in CERBERE).

    Args:
        args: the arguments.

    """
    static["args"] = copy.deepcopy(args)
    # This will look for every scenario.json in the directory and create Vagrant/Ansible files to go with it
    # Get the list of all input json files
    scenario_paths = glob.glob(args.output_directory + "/scenario_*/output_scenario.json")

    log.info("Sorting input list alphabetically...")
    # lambda wizardry needed so that 2 happens before 10
    scenario_paths.sort(key=lambda f: int(''.join(filter(str.isdigit, f))))
    vm_name_already_used = []
    j = 0
    for scenario_path in scenario_paths:
        dest_dir = os.path.dirname(scenario_path)
        verify_target_dir(dest_dir=os.path.dirname(scenario_path))

        log.info("Parsing scenario...")
        vms = parser.parse_vms_from_json(scenario_path, j, vm_name_already_used, len(scenario_paths),
                                         args.ignore_virtualbox)

        if not args.ignore_virtualbox:
            log.info("Generating Vagrantfile...")
            vg.generate_vagrantfile(vms, dest_dir)

        # log.info("Generating yaml...")  # WAS USEFUL FOR IRL CERBERE EXPERIMENT
        # vg.generate_yaml(vms, dest_dir)

        log.info("Generating Ansible roles...")

        log.info("Generating Ansible playbooks...")
        ag.generate_all_vm_roles(vms, dest_dir,
                                 args.ignore_virtualbox)  # simplify heavily it's all gonna move to roles.

        # delete generate files
        mypath = f"{os.path.dirname(scenario_path)}/generated_files"
        onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
        if len(onlyfiles) == 0:
            shutil.rmtree(mypath)

        # create a fused playbook
        # it is not referenced by the vagrantfile but is useful for niche applications

        playbooks = glob.glob(dest_dir + "/main_playbook_*.yml")
        with open(dest_dir + "/main_playbook.yml", "w") as out_file:
            for playbook in playbooks:
                with open(playbook, "r") as in_file:
                    content = in_file.read()
                out_file.write(content)

        # if args.separate_tasks:
        #     log.info("Generating additional playbooks separating tasks.")
        #     playbook_files = glob.glob(dest_dir + "/main_playbook*")
        #     for playbook in playbook_files:
        #         task_dest_dir = dest_dir + "/tasks/"
        #         os.mkdir(task_dest_dir)
        #         base_playbook = os.path.basename(playbook)
        #         end_name = base_playbook.replace("main_playbook_", "")
        #         task_playbook_path = task_dest_dir + "/task_playbook_" + end_name
        #         post_task_playbook_path = task_dest_dir + "/post_task_playbook_" + end_name
        #
        #         with open(playbook, "r") as inFile:
        #             whole_playbook = yaml.safe_load(inFile.read())
        #             task_playbook = whole_playbook[0]["tasks"]
        #             post_task_playbook = whole_playbook[0]["post_tasks"]
        #         with open(task_playbook_path, "w+") as outFile:
        #             yaml.dump(task_playbook, outFile, default_flow_style=False)
        #         with open(post_task_playbook_path, "w+") as outFile:
        #             yaml.dump(post_task_playbook, outFile, default_flow_style=False)
        #
        # log.info("Generating post launch scripts...")
        # plscripts.post_launch_payload_detonation(vms)

        print(blue("\nApiculteur is done."))
        print(
            f"""
       To use the output you can run the following in your terminal :

       cd {dest_dir} && vagrant up
       """
        )
        j += 1
