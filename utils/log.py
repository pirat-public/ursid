import logging
import copy
from utils.colors import *


project_title_unicode = """
                            
 _   _ ____  ____ ___ ____  
| | | |  _ \/ ___|_ _|  _ \ 
| | | | |_) \___ \| || | | |
| |_| |  _ < ___) | || |_| |
 \___/|_| \_|____|___|____/ 
                            
"""


class DefaultConsoleHandler(logging.StreamHandler):
    def emit(self, record: logging.LogRecord):
        colored = copy.copy(record)

        if record.levelname == "WARNING":
            colored.msg = yellow(record.msg)
            colored.levelname = yellow(record.levelname)
        elif record.levelname == "CRITICAL" or record.levelname == "ERROR":
            colored.msg = red(record.msg)
            colored.levelname = red(record.levelname)
        elif record.levelname == "DEBUG":
            colored.msg = magenta(record.msg)
            colored.levelname = magenta(record.levelname)
        else:
            colored.msg = record.msg

        logging.StreamHandler.emit(self, colored)


def init_logging(args):
    log_format = "[%(asctime)s] %(levelname)-8s %(name)-12s %(message)s"
    if args.verbose:
        logging.basicConfig(
            level=logging.DEBUG,
            format=log_format,
            handlers=[DefaultConsoleHandler()],
            datefmt="%d/%m/%Y %I:%M:%S",
        )
    else:
        logging.basicConfig(
            level=logging.INFO,
            format=log_format,
            handlers=[DefaultConsoleHandler()],
            datefmt="%d/%m/%Y %I:%M:%S",
        )
    print(yellow(project_title_unicode))
