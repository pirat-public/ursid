import logging

log = logging.getLogger(__name__)

ask_user = True


def user_confirmation(question: str) -> bool:
    """
    Takes a yes/no question and asks for user input to confirm
    Note that if the module wide variable `ask_user` was set to False, the
    user prompt will be skipped and a warning log will be generated instead.

    Args:
        question (str): [description]

    Returns:
        bool:   `True`    ->    the user confirmed
                `False`   ->    the user denied
    """
    if ask_user:
        response = input(f"{question} [y/n]: ")
        response = response.split("\n")[0].lower()
        if response == "y" or response == "yes":
            return True
        return False
    else:
        log.warning(f"{question} [y/n]: (auto-accept)")
        return True
