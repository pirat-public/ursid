import logging
from traceback import TracebackException

log = logging.getLogger(__name__)


def indent_str(to_write: str, indent: int = 4):
    indent_str = " " * indent
    ret_text = ""
    for line in to_write.splitlines(True):
        ret_text += f"{indent_str}{line}"
    return ret_text


def is_cpe(cpe: str) -> bool:
    cpe_items = cpe.split(":")
    if len(cpe_items) > 2 and cpe_items[0] == "cpe":
        return True
    return False


def parse_cpe(cpe: str) -> dict:
    """
    Source : https://en.wikipedia.org/wiki/Common_Platform_Enumeration

    cpe:<cpe_version>:<part>:<vendor>:<product>:<version>:<update>:<edition>:<language>:<sw_edition>:<target_sw>:<target_hw>:<other>

    part:

        a for applications,

        h for hardware platforms,

        o for operating systems.

    Returns:
        {
            "part" : "a" | "h" | "o",
            "vendor" : str,
            "product" : str,
            "version" : str
        }
    """
    parsed_cpe = {}
    expected_cpe_version = "2.3"
    minimum_length = 6
    cpe_items = cpe.split(":")
    if len(cpe_items) < minimum_length:
        log.error(
            f"Error parsing CPE: {cpe}. Expected at least {minimum_length} parts."
        )
        exit(1)
    if cpe_items[0] != "cpe":
        log.error(
            f'Error parsing CPE: {cpe}. Expected "cpe" and parsed {cpe_items[0]}.'
        )
        exit(1)
    if cpe_items[1] != expected_cpe_version:
        log.warning(
            f"Parsing CPE of unknown version. Expected {expected_cpe_version} and parsed {cpe_items[1]}."
        )
    part = cpe_items[2]
    if part != "a" and part != "h" and part != "o":
        log.error(
            f'Error parsing cpe: {cpe}. Expected "a" or "o" or "h" and parsed {part}.'
        )
        exit(1)
    parsed_cpe["part"] = part
    parsed_cpe["vendor"] = cpe_items[3]
    parsed_cpe["product"] = cpe_items[4]
    parsed_cpe["version"] = cpe_items[5]
    return parsed_cpe


def parse_version(v: str) -> "tuple[int,int,int]":
    v = [int(_) for _ in v.split(".")]
    if len(v) > 3:
        log.error(
            f'Error parsing versions "{v}". Please make sure they are correct. If they are consider updating the code in this module.'
        )
        exit(1)
    return v + (3 - len(v)) * [0]


def find_closest_version(version1: str, versions: "list[str]") -> "str | False":
    """Compare 2 version strings.
    Versions can be in the following formats:
        "1.2.3"
        "1.2"
        "1"
    For instance a version "1" will be transposed
    to "1.0.0".

    Return:
        False if an error occurs
        [int, int, int] containing the signed differences (v1 - v2)
    """
    if len(versions) == 0:
        return False
    try:
        v1 = parse_version(version1)
        log.debug(f'Parsing versions of "{versions}"')
        versions_parsed = [parse_version(_) for _ in versions]

        closest = versions_parsed[0]
        diff = [
            v1[0] - closest[0],
            v1[1] - closest[1],
            v1[2] - closest[2],
        ]
        for v in versions_parsed:
            log.debug(closest)
            if (bd := abs(v1[0] - v[0])) < abs(diff[0]):
                diff = [
                    v1[0] - v[0],
                    v1[1] - v[1],
                    v1[2] - v[2],
                ]
                closest = v
            elif bd == abs(diff[0]):
                if (md := abs(v1[1] - v[1])) < abs(diff[1]):
                    diff = [
                        v1[0] - v[0],
                        v1[1] - v[1],
                        v1[2] - v[2],
                    ]
                    closest = v
                elif md == abs(diff[1]):
                    if abs(v1[2] - v[2]) < abs(diff[2]):
                        diff = [
                            v1[0] - v[0],
                            v1[1] - v[1],
                            v1[2] - v[2],
                        ]
                        closest = v
        return versions[versions_parsed.index(closest)]
    except Exception as e:
        log.error(
            f'Error trying to find closest version of "{version1}" in "{versions}".'
        )
        log.error(e, exc_info=True)
        exit(1)


assert find_closest_version("1.0.2", ["7.1.9", "1.0.1", "1.2.2", "3.2.1"]) == "1.0.1"
