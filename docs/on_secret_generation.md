# How secret generation works

A secret dictionary is initialized at the beginning of the refinement process. This dictionary will be filled with
Secret objects defined in secret_generation.py, and consist of a name, type and value (null by default). The name is
also used as a key for this dictionary.

Whenever a transition is chosen to be refined, that transition may require or reward secrets. Only procedures that
have matching Secret preconditions will be picked. These secret preconditions indicate which type of secrets a procedure
may deal with (ssh keys, plaintext passwords...) and how much.

While theoretically we could come up with procedures that require or reward several kind of secrets at once (which is
why Secret Preconditions is made of lists), in practice these cases are a) rare b) annoying to deal with c) may just
be handled by adding additional transitions on the graph instead. Thus, when adding new procedures, one should not have
more than one entry in each of Secret Precondition sublists.

Once a procedure is picked, an ArchitecturalConstraints object corresponding to the constraints of that procedure will
be generated. This object will also remember which secrets the transition associated to that procedure dealt with.

Secrets may be used either by FileConstraints objects (which may need it to fill file contents, for instance to leak
a password), or AccountConstraint objects (which set user account names).
At the end of the refinement process, all of these constraints are iterated upon. Since the ArchitecturalConstraints
object remembers the name of the secrets it was associated with, it is possible to look for those in the secret dictionary.
Thus for every secret of every ArchitecturalConstraint, its named will be search in the dictionary. If it does not have a
value yet, its value will be generated (using secret_generation.py). If it does have a value already, that value is reused.
This ensures that values generated in one part of the scenario will be properly reused in other parts if needed.

Note: this might have irregular behavior on procedure which would have both file and account constraints requiring secrets,
but that case hasn't presented itself yet.