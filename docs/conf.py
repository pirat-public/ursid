import os
import sys

sys.path.insert(0, os.path.abspath('.'))
sys.path.insert(0, os.path.abspath("../"))
project = "URSID"

author = "Pierre-Victor BESSON"
extensions = [
    "myst_parser",
    "sphinx.ext.autodoc",
    'sphinx_rtd_theme',
    "sphinx.ext.napoleon",
    "sphinx_autodoc_typehints",
    "sphinx.ext.autosectionlabel"
]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store", ".venv"]
templates_path = ["_templates"]
html_theme = "sphinx_rtd_theme"
autosectionlabel_prefix_document = True
