# Provided procedures.
The currently available procedures are all relevant to the CERBERE example scenario. More may be added in the future.

## Tactic: Initial Access
### Technique T1190: [Exploit Public Facing Application](https://attack.mitre.org/techniques/T1190/)
#### Website with command injection (easy)
- A simple npm website running on port 3000 containing a command injection, which in particular can be used to get a reverse shell logged in as the user which ran the website.
- The command injection is contained in the image search bar on a user profile. In order to access it, an attacker needs to create an account and login.
- Any command after a pipe will be executed directly as the user. For instance, running "hop | ls" will display the content of the folder where the website is ran from.
- Also has additional functions such as account creation, login, and image upload.
- Contains a few red-herrings.

#### Website with command injection (medium)
- Same website, except the command injection now has a limit of 50 characters.
- This can be bypassed by using the upload feature on the website to upload a reverse shell script.
- Then change permissions and execute the script using the injection in order to get your reverse shell.

#### Django directory traversal rewarding ssh key (debian)
- A simple django website providing an admin login page on port 8000. If the attacker manages to log in as an admin, they may perform a simple directory traversal to find the /notes directory containing a SSH key.
- This procedures requires a password to be chosen, and rewards a SSH key.

#### Django directory traversal rewarding ssh key (ubuntu)
- The same procedure but on ubuntu.
- The 2 procedures are separated (instead of being grouped as a single one with a *linux* OS constraint) in order to manually specify the OS versions they can be ran on for ubuntu and debian separately.

## Tactic: Privilege Escalation
### Technique T1068: [Exploitation for Privilege Escalation](https://attack.mitre.org/techniques/T1068/)
#### Vulnerable sudo version (CVE-2019-14287)
- An underflow bug in old sudo versions (here 1.8.27) gives attacker root privileges if the sudoers file was configured in a particular way.
- More information can be found [here](https://www.exploit-db.com/exploits/47502)

#### Vulnerable pkexec process
- CVE-2021–4034, which affects most old UNIX systems, allows attackers to gain root privileges by running a script abusing the pkexec linux executable.
- In particular the default Vagrant box for ubuntu 16.04 is vulnerable, which makes this vulnerability very simple to implement.
- Probably works on other old linux versions too.
- [Example exploit](https://github.com/ly4k/PwnKit).

## Tactic: Credential Access
### Technique T1552: [Unsecured Credentials](https://attack.mitre.org/techniques/T1552/)
#### Passwords in .bash_history
- Stores a secret in the .bash_history of the relevant user.
- This .bash_history file is for now not really realistic, and will just contain the credential.
- Requires a Password secret.

#### Password in .txt file
- Stores a secret in a file named important.txt in the home directory of the relevant user.
- Requires a PASSWORD secret.

#### Private Keys in .ssh
- Adds a private key to the .ssh/authorized_keys file of the home directory of the relevant user.
- Requires a SSH_KEYS secret.
- Unused in CERBERE.

## Tactic: Lateral Movement
### Technique T1021: [Remote Services](https://attack.mitre.org/techniques/T1021/)
#### Weak SSH password
- Sets the relevant user's password to an easy to crack value (taken from rockyou.txt), and edits the sudo configuration file in order to make remote password authentication possible.
- Does not require any secret.
- Not used in CERBERE.
#### Medium SSH password
- Same as above, except the passwords are a bit harder.
- Not used in CERBERE
#### SSH Access from key
- Sets a private/public key pair configuration to make the user accessible through SSH.
- Requires a SSH_KEYS secret.
#### SSH Access from password
- Sets the relevant user's password to a fixed value,  and edits the sudo configuration file in order to make remote password authentication possible.
- Requires a PASSWORD secret.
- Not used in CERBERE
#### SQL server rewarding a flag
- Installs and launches a postgresql database on the machine, remotely accessible through port 5432.
- This database is remotely accessible through password authentication, and contains a flag for the attacker to extract (here a path to a local image).
- Requires a PASSWORD secret.