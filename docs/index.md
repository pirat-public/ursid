# URSID Homepage

Welcome to the URSID documentation!

## What is URSID

URSID is an **automatic vulnerable architecture generation** tool. By describing an attack scenario on a high-level
(using the [MITRE](https://attack.mitre.org/) technique nomenclature), URSID is able to refine it into several on a
procedural level.
All of the resulting architectures will be vulnerable to the same attack on a technical level, but procedures
(such as exploits available on machines) and secrets (such as passwords) will vary on a machine to machine basis.

Uses for URSID as a tool for researchers include:

- Customized honeypot generation, for instance in order to match a known attacker's prefered modus operandi.
- Cyber-range/CTF/red-team training applications, including AI attackers.
- Log and dataset generation corresponding to specific attacks.

## Requirements

- Python 3.10 or later.

If you wish to also deploy the generated architectures:

- Vagrant 2.2.19 or later.
- Ansible. 2.12 or later.
- VirtualBox v6.1.32 or later.

All tests of URSID were ran on Ubuntu 20.04.

## How URSID works

URSID is comprised of 2 parts:

- The Chouchen module, which refines scenarios from a technical level to a procedural level.
- The Apiculteur module, which converts scenario described on a procedural level them into Vagrantfile and Ansible
  playbooks.

The following image summarizes the overall structure of URSID
![](./imgs/ursid_structure.png)

## Available examples

This repository comes with a few example procedures, which may be added and deployed into scenarios at will.
You may find more information about those [here] (#provided_procedures).

This repository also provides an example attack scenario in the form of the [CERBERE](#cerbere_scenario) scenario.
This scenario is made of 3 machines including 2 simple vulnerable websites, logging tools, and comes with detailed
attack instructions.

## Try it yourself!

To just generate a single procedural scenario associated with the CERBERE scenario, run

```bash
python3 main.py ./example_scenario/cerbere_json.json --refinement_only
```

If you wish to generate more than one in order to showcase variation, run

```bash
python3 main.py ./example_scenario/cerbere_json.json -n YOURNUMBER --refinement_only
```

The generated scenarios will be available in the ./output directory. output_scenario.json files will contain
informations
about each machine of the scenario, and the generated_files directory will contain the files generated for each
machines.
Feel free to browse those and compare them between each generation in order to see the procedural refinement at work.

If you wish to also generate Ansible playbooks and Vagrantfiles (requires correct version of Virtualbox, Vagrant and
Ansible installed), run

```bash
python3 main.py ./example_scenario/cerbere_json.json -n YOURNUMBER
```

If you then wish to deploy one of the instances locally (for instance the first one generated), run

```bash
cd ./output/scenario_0/
vagrant up
```

Note that in the publicly available implementation, different scenarios instances will be able to see each other on the
network.

If you wish to add your own procedures, refer to [](#adding_procedures).
If you wish to create your own scenario (using prebuilt procedures or your own), refer
to [](#creating_your_own_scenario).
Once your scenario is in a .json, run

```bash
python3 main.py PATH_TO_YOUR_JSON -n NUMBER_OF_SCENARIOS_TO_REFINE
```

The project will warn you if the /output directory is not empty.

URSID also offers a proof-of-concept implementation of the refinement process inside a docker container.
In order to refine the CERBERE scenario 5 times and get the resulting files in the REP_PATH directory, run

```bash
sudo docker pull ursidfinal/ursid
sudo docker run -v REP_PATH:/ursid-final/output ursidfinal/ursid
```

## Contributors

- Main contributor and repository maintainer: Pierre-Victor BESSON, PhD student at CentraleSupelec/INRIA.
- Design contributions: Valérie VIET TRIEM TONG, Gilles GUETTE, Erwan ABGRALL and Guillaume PIOLLE, all PhD supervisors.
- Code contribution (Apiculteur): Gireg Maury and Alexandre Monroche, former CentraleSupelec students.
- Special thanks: Alexandre Sanchez.

Questions? Contact pierre-victor.besson@inria.fr

```{toctree}
:maxdepth: 2
:caption: "Contents:"

provided_procedures
adding_procedures
cerbere_scenario
creating_your_own_scenario
on_secret_generation
chouchen_api
apiculteur_api
```

Icon references: <a href="https://www.flaticon.com/free-icons/mead" title="mead icons">Mead icons created by Flat
Icons - Flaticon</a>
<a href="https://www.flaticon.com/free-icons/bee" title="bee icons">Bee icons created by justicon - Flaticon</a>