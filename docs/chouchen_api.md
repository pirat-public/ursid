# About `chouchen`

An overview of the purpose of each module is as follows:
- system_graph describe the scenario on a technical level (using attack graphs).
- graphics offers graphical representation of system graphs.
- constraints describes architectural constraints associated to procedure and how they are combined.
- subconstraints helps with that.
- file_generation generates files to populate the resulting machines.
- secret_generation generates secrets to populate the resulting machines (passwords, keys...).
- procedural_refinement combines all the above modules to generate a procedural-level scenario based on a technical-level description.

This API may be implemented as a PyPi library in the future.

## chouchen.system_graph

```{eval-rst}
.. automodule:: chouchen.system_graph
  :members:
```

## chouchen.graphics

```{eval-rst}
.. automodule:: chouchen.graphics
  :members:
```

## chouchen.constraints

```{eval-rst}
.. automodule:: chouchen.constraints
  :members:
```

## chouchen.file_generation

```{eval-rst}
.. automodule:: chouchen.file_generation
  :members:
```