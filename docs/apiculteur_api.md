# About `apiculteur`

An overview of the purpose of each module is as follows:
- ansible_generation handles writing Ansible playbooks and detonating payload scripts.
- parsing handles CPE parsing, describing VMs and checking json schemas.
- vagrant_generation handles writing Vagrantfiles.
- virtualbox_interface provides functions interacting with Virtualbox (for instance to delete VMs or check which
networks are available).

This API may be implemented as a PyPi library in the future.


## apiculteur

```{eval-rst}
.. automodule:: apiculteur.main
  :members:
```

```{eval-rst}
.. automodule:: apiculteur.ansible_generation.ansible_generator
  :members:
```


```{eval-rst}
.. automodule:: apiculteur.ansible_generation.interface_vagrant
  :members:
```


```{eval-rst}
.. automodule:: apiculteur.ansible_generation.post_launch_scripts
  :members:
```

```{eval-rst}
.. automodule:: apiculteur.parsing.parser
  :members:
```

```{eval-rst}
.. automodule:: apiculteur.parsing.validator
  :members:
```

```{eval-rst}
.. automodule:: apiculteur.parsing.VM
  :members:
```

```{eval-rst}
.. automodule:: apiculteur.vagrant_generation.interface_ansible
  :members:
```

```{eval-rst}
.. automodule:: apiculteur.vagrant_generation.vagrant_generator
  :members:
```

```{eval-rst}
.. automodule:: apiculteur.virtualbox_interface.manage
  :members:
```

```{eval-rst}
.. automodule:: apiculteur.virtualbox_interface.VNM
  :members:
```



