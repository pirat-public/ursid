## How to write your own scenario json files

- Use gui.py (recommended).
- Write your own scenario in python then export using the system_graph class (harder).
    - Example template in gui.py/scenario_cerbere().
- Write the json manually using breizh_ctf.json and cerbere.json as templates (more annoying).

## How to add configuration to each machine

- URSID will read additional configuration in a .yml file.
    - It has to be the same name as the json file. For instance, to add configuration to cerbere.json write it in
      cerbere.yml
    - Use cerbere.yml as an example.
    - Unrecognized configurations will raise a warning log but won't stop the software.
- Available configurations:
    - options:
        - **auditd**: installs an auditd on the machine.
    - networks:
        - Write network names the machine should be in (can be in more than one).